import React from 'react';
import { useTheme, ThemeProvider, createTheme } from '@mui/material/styles';
import LoginPage from './pages/LoginPage';
import MainPage from './pages/MainPage';
import { Routes, Route, Navigate } from 'react-router-dom';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { CircularProgress, Stack } from '@mui/material';
import SettingsPage from './pages/SettingsPage';
import SuggestionsPage from './pages/SuggestionsPage';
import SearchPage from './pages/SearchPage';
import MoviePage from './pages/MoviePage';
import CrewPage from './pages/CrewPage';
import UserPage from './pages/UserPage';
import CompanyPage from './pages/CompanyPage';
import CreditsPage from './pages/CreditsPage';
import NavBar from './components/NavBar/NavBar';
import Communication from './service/communication';
import useMediaQuery from '@mui/material/useMediaQuery';

export default function App() {
    const [isAuthed, setAuthed] = React.useState(undefined);
    const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)') ? 'dark' : 'light';
    const [mode, setMode] = React.useState(localStorage.getItem('themeMode') || prefersDarkMode);

    React.useEffect(() => {
        onAuthStateChanged(getAuth(), async (user) => {
            if (user) {
                if (!isAuthed) {
                    await Communication.sendGetRequest('api/public/login/', await user.getIdToken())
                        .then(async (response) => {
                            if (response.status === 200) {
                                setAuthed(true);
                            }
                        });
                }
            } else {
                setAuthed(false);
            }
        });
    }, []);

    const getCurrentMode = () => {
        return createTheme(getDesignTokens(mode));
    }

    const changeCurrentMode = () => {
        const newMode = mode === 'light' ? 'dark' : 'light';
        setMode(newMode);
        localStorage.setItem('themeMode', newMode);
    }

    const getDesignTokens = (mode) => ({
        palette: {
            mode,
            ...(mode === 'dark'
                ? {
                    background: {
                        default: 'black',
                        paper: 'black',
                        movieResult: '#303030'
                    }
                }
                : {
                    background: {
                        default: 'white',
                        paper: 'white',
                        movieResult: '#FAFAFA'
                    }
                }
            ),
            text: {
                ...(mode === 'dark'
                    ? {
                        primary: '#FFFFFF',
                        secondary: '#808080'
                    }
                    : {
                        primary: '#000000',
                        secondary: '#808080'
                    }
                )
            }
        }
    });

    if (isAuthed === undefined) {
        return <AppLoading currentMode={getCurrentMode()} />;
    }

    return (
        <ThemeProvider theme={getCurrentMode()}>
            <Routes>
                <Route path='/Login' element={!isAuthed ? <LoginPage /> : <Navigate to='/' />} />
                <Route path='/*' element={
                    <RequireAuth redirectTo='/Login' authState={isAuthed}>
                        <MainAppRouter changeCurrentMode={changeCurrentMode} />
                    </RequireAuth>
                } />
            </Routes>
        </ThemeProvider>
    );
}

function RequireAuth({ children, redirectTo, authState }) {
    return authState ? children : <Navigate to={redirectTo} />;
}

function MainAppRouter(props) {
    const theme = useTheme();
    return (
        <div style={{ minHeight: 'inherit', marginBottom: 20, backgroundColor: theme.palette.background.default, color: theme.palette.text.primary }}>
            <NavBar toggleColorMode={props.changeCurrentMode} />
            <Routes>
                <Route path='/' element={<MainPage />} />
                <Route path='/Search/:searchText' element={<SearchPage />} />
                <Route path='/Movie/:movieID' element={<MoviePage />} />
                <Route path='/Crew/:personID' element={<CrewPage />} />
                <Route path='/User/:id' element={<UserPage />} />
                <Route path='/User' element={<UserPage />} />
                <Route path='/Company/:companyID' element={<CompanyPage />} />
                <Route path='/Settings' element={<SettingsPage />} />
                <Route path='/Suggestions' element={<SuggestionsPage />} />
                <Route path='/Credits' element={<CreditsPage />} />
            </Routes>
        </div>
    );
}

function AppLoading(props) {
    return (
        <ThemeProvider theme={props.currentMode}>
            <Stack alignItems='center'>
                <CircularProgress></CircularProgress>
            </Stack>
        </ThemeProvider>
    );
}
