import { Carousel } from '@trendyol-js/react-carousel';
import { styled } from '@mui/material/styles';

export default styled(Carousel)(({ theme }) => ({
    'width': '100%',
    '.styles-module_carousel-arrow__26sRw': {
        'backgroundColor': theme.palette.background.movieResult,
        'width': '50px',
        'height': '100%',
        'border': 'none'
    },
    '.styles-module_carousel-arrow__26sRw:before': {
        'content': 'url("../images/slick-arrow.svg")'
    },
    '.styles-module_carousel-arrow__26sRw[data-direction="left"]': {
        'transform': 'rotateZ(180deg)'
    }
}));
