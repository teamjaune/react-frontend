import * as React from 'react';
import * as Mui from '@mui/material';
import { getAuth } from 'firebase/auth';
import { styled } from '@mui/material/styles';
import Communication from '../service/communication';
import RatingObj from '../httpObjects/Rating';

export default function RatingDisplay(props) {
    const [rating, setRating] = React.useState(props.rating);
    const Rating = styled(Mui.Rating)(({ _ }) => ({
        '& .css-dqr9h-MuiRating-label': {
            'top': '3.75px'
        }
    }));
    const RatingReadOnly = styled(Mui.Rating)(({ _ }) => ({
        '< div': {
            'marginTop': '-5px'
        },
        '& .css-dqr9h-MuiRating-label': {
            'top': '3.75px'
        }
    }));

    const onRate = (_, newValue) => {
        if (!props.readOnly) {
            getAuth().currentUser.getIdToken().then(token => {
                if (newValue === null) {
                    Communication.sendDeleteRequest('api/private/rating', token, props.movie.movie_id).then(async (response) => {
                        if (response.status === 200) {
                            setRating(0);
                            props.movie.our_rating = 0;
                        }
                    }).catch((e) => {
                        console.error(e);
                    });
                } else {
                    Communication.sendPutRequest('api/private/rating', token, new RatingObj(props.movie.movie_id, (newValue * 2))).then(async (response) => {
                        const result = await response.json();
                        setRating(result.rating / 2);
                        props.movie.our_rating = result.rating;
                    }).catch((e) => {
                        console.error(e);
                    });
                }
            });
        }
    };

    const buildRatingComponent = (isReadOnly) => {
        const precision = 0.5;
        const value = rating;
        const size = 'large';
        const style = { color: props.color };
        if (isReadOnly) {
            if (value <= 0) {
                return <Mui.Typography variant='h5'>Not rated yet</Mui.Typography>
            }
            return <RatingReadOnly style={style} size={size} precision={precision} value={value} readOnly></RatingReadOnly>
        }
        return <Rating onChange={onRate} style={style} size={size} precision={precision} value={value}></Rating>
    }
    return (
        <>
            <Mui.Stack direction="column"
                justifyContent="flex-start"
                alignItems="center"
                style={{ marginTop: (props.readOnly ? '-10px' : '0') }}
            >
                <Mui.Typography variant='h5'>{props.title}</Mui.Typography>
                <Mui.Stack direction="row"
                    justifyContent="flex-start"
                    alignItems="center"
                    spacing={1}
                >
                    {buildRatingComponent(props.readOnly)}
                </Mui.Stack>
            </Mui.Stack>
        </>
    );
}
