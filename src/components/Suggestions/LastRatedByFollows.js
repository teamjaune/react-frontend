import * as React from 'react';
import * as Mui from '@mui/material';
import { getAuth } from 'firebase/auth';
import Communication from '../../service/communication';
import HowToWatchCarousel from '../HowToWatchCarousel';
import MovieCard from '../Cards/MovieCard';
import SearchMovie from '../../httpObjects/Search/SearchMovie';
import LoadingCarousel from '../LoadingCarousel';
import { useTranslation } from 'react-i18next';

export default function SuggestionsPage() {
    const [moviesloading, setMoviesLoading] = React.useState(true);
    const [followsLastRatings, setFollowsLastRatings] = React.useState([]);
    const [t] = useTranslation('common');

    React.useEffect(() => {
        getAuth().currentUser.getIdToken().then(token => {
            getLastRatedMoviesByFollows(token);
        });
    }, []);

    const getLastRatedMoviesByFollows = (token) => {
        Communication.sendGetRequest('api/private/suggestions/followingsSuggestions', token).then(async (response) => {
            if (response.status !== 200) {
                setFollowsLastRatings([]);
                setMoviesLoading(false);
            } else {
                const result = await response.json();
                if (result === null || !Array.isArray(result)) {
                    setFollowsLastRatings([]);
                    setMoviesLoading(false);
                } else {
                    const res = [];
                    for (const movie of result) {
                        res.push(new SearchMovie(movie));
                    }
                    setFollowsLastRatings(res);
                    setMoviesLoading(false);
                }
            }
        }).catch((_) => {
            setFollowsLastRatings([]);
            setMoviesLoading(false);
        });
    }

    const getMoviesFromFollowsLastRatings = () => {
        const movies = [];
        for (const movie of followsLastRatings) {
            movies.push(<MovieCard key={movie.movie_id} movie={movie}></MovieCard>);
        }
        return movies;
    }

    const displayMoviesFromFollowsLastRatings = () => {
        const res = [];
        res.push(
            <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1} key={'followsSuggestionsText'}>
                <Mui.Box p={1}>
                    <Mui.Typography variant="h4" gutterBottom>{t('suggestions.follows.text')}</Mui.Typography>
                </Mui.Box>
            </Mui.Box>
        );

        if (moviesloading) {
            res.push(
                <LoadingCarousel key={'followsSuggestionsLoadingCarousel'} cardKey={'lastRatedByFollows'} slidesToShow={5}></LoadingCarousel>
            );
        } else {
            const moviesFromFollowsLastRating = getMoviesFromFollowsLastRatings();
            if (moviesFromFollowsLastRating.length === 0) {
                res.push(
                    <Mui.Box key={'followsSuggestionsNothingFound'} display="block" mx={10} p={1}>
                        <Mui.Typography variant="h4" textAlign={'center'} gutterBottom>{t('suggestions.follows.nothing')}</Mui.Typography>
                    </Mui.Box>
                );
                return res;
            }
            let nbOfSlidesToScroll = 5;
            while (moviesFromFollowsLastRating.length % nbOfSlidesToScroll !== 0) {
                nbOfSlidesToScroll--;
            }
            const settings = {
                infinite: true,
                slidesToShow: 5,
                slidesToScroll: nbOfSlidesToScroll
            };

            res.push(
                <Mui.Box key={'followsSuggestionsCarousel'} display="block" mx={10} p={1}>
                    <HowToWatchCarousel {...settings}>
                        {moviesFromFollowsLastRating}
                    </HowToWatchCarousel>
                </Mui.Box>
            );
        }
        return res;
    };

  return (
    <>
      {displayMoviesFromFollowsLastRatings()}
    </>
  );
}
