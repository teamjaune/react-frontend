import * as React from 'react';
import * as Mui from '@mui/material';
import { getAuth } from 'firebase/auth';
import Communication from '../../service/communication';
import HowToWatchCarousel from '../HowToWatchCarousel';
import MovieCard from '../Cards/MovieCard';
import SearchMovie from '../../httpObjects/Search/SearchMovie';
import LoadingCarousel from '../LoadingCarousel';
import { useTranslation } from 'react-i18next';

export default function SuggestionsPage() {
    const [globalSuggestionsloading, setGlobalSuggestionsLoading] = React.useState(true);
    const [globalSuggestions, setGlobalSuggestions] = React.useState([]);
    const [t] = useTranslation('common');

    React.useEffect(() => {
        getAuth().currentUser.getIdToken().then(token => {
            getSuggestionsForAllMovies(token);
        });
    }, []);

    const getSuggestionsForAllMovies = (token) => {
        Communication.sendGetRequest('api/private/suggestions/globalSuggestions', token).then(async (response) => {
            if (response.status !== 200) {
                setGlobalSuggestions([]);
                setGlobalSuggestionsLoading(false);
            } else {
                const result = await response.json();
                if (result === null || !Array.isArray(result)) {
                    setGlobalSuggestions([]);
                    setGlobalSuggestionsLoading(false);
                } else {
                    const res = [];
                    for (const movie of result) {
                        res.push(new SearchMovie(movie));
                    }
                    setGlobalSuggestions(res);
                    setGlobalSuggestionsLoading(false);
                }
            }
        }).catch((_) => {
            setGlobalSuggestions([]);
            setGlobalSuggestionsLoading(false);
        });
    }

    const getGlobalSuggestionsMovies = () => {
        const movies = [];
        for (const movie of globalSuggestions) {
            movies.push(<MovieCard key={movie.movie_id} movie={movie}></MovieCard>);
        }
        return movies;
    }

    const displayGlobalSuggestionsMovies = () => {
        const res = [];
        res.push(
            <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1} key={'globalSuggestionsText'}>
                    <Mui.Typography variant="h4" gutterBottom>{t('suggestions.global.text')}</Mui.Typography>
            </Mui.Box>
        );

        if (globalSuggestionsloading) {
            res.push(
                <LoadingCarousel key={'globalSuggestionsLoadingCarousel'} cardKey={'GlobalSuggestions'} slidesToShow={5}></LoadingCarousel>
            );
        } else {
            const globalSuggestionsMovies = getGlobalSuggestionsMovies();
            let nbOfSlidesToScroll = 5;
            while (globalSuggestionsMovies.length % nbOfSlidesToScroll !== 0) {
                nbOfSlidesToScroll--;
            }
            const settings = {
                infinite: true,
                slidesToShow: 5,
                slidesToScroll: nbOfSlidesToScroll
            };
            res.push(
                <Mui.Box key={'globalSuggestionsCarousel'} display="block" mx={10} p={1}>
                    <HowToWatchCarousel {...settings}>
                        {globalSuggestionsMovies}
                    </HowToWatchCarousel>
                </Mui.Box>
            );
        }
        return res;
    };

  return (
    <>
      {displayGlobalSuggestionsMovies()}
    </>
  );
}
