import * as React from 'react';
import * as Mui from '@mui/material';
import { getAuth } from 'firebase/auth';
import Communication from '../../service/communication';
import HowToWatchCarousel from '../HowToWatchCarousel';
import MovieCard from '../Cards/MovieCard';
import SearchMovie from '../../httpObjects/Search/SearchMovie';
import Movie from '../../httpObjects/Movie';
import { styled, useTheme } from '@mui/material/styles';
import LoadingCarousel from '../LoadingCarousel';
import { useTranslation } from 'react-i18next';

export default function SuggestionsPage(props) {
    const theme = useTheme();
    const [t] = useTranslation('common');

    const MovieLink = styled(Mui.Link)(({ _ }) => ({
        'fontWeight': 'bold',
        'color': theme.palette.text.primary,
        'textDecoration': 'none',
        '&:hover': {
            'fontWeight': 'bold',
            'color': theme.palette.text.primary,
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    const NUMBER_OF_SUGGESTIONS = props.numberOfSuggestions;
    const OFFSET = props.offset;
    const [lastRatedSuggestions, setLastRatedSuggestions] = React.useState([]);
    const [lastRatedMovies, setLastRatedMovies] = React.useState([]);
    const [pendingReceive, setPendingReceive] = React.useState(false);

    React.useEffect(() => {
        getAuth().currentUser.getIdToken().then(token => {
            for (let i = 0; i < NUMBER_OF_SUGGESTIONS; i++) {
                getSuggestionsForTheLastMovieAtIndex(token, i + OFFSET);
            }
        });
    }, []);

    const handleBadResponse = () => {
        const resSuggestions = lastRatedSuggestions;
        const resLastRatedMovies = lastRatedMovies;
        resLastRatedMovies.push(null)
        resSuggestions.push(null);
        setLastRatedSuggestions(resSuggestions);
        setLastRatedMovies(resLastRatedMovies);
        setPendingReceive(false);
    }

    const getSuggestionsForTheLastMovieAtIndex = (token, index) => {
        Communication.sendGetRequest('api/private/suggestions/lastRatedSuggestions/' + index, token).then(async (response) => {
          while (pendingReceive);//eslint-disable-line
          setPendingReceive(true);
          if (response.status !== 200) {
            handleBadResponse();
          } else {
            const result = await response.json();
            if (result !== null || result.last_rated_movie !== null || result.suggestions !== null || Array.isArray(result.suggestions)) {
              const resSuggestions = lastRatedSuggestions;
              const resLastRatedMovies = lastRatedMovies;
              resLastRatedMovies.push(new Movie(result.last_rated_movie))
              const currentSuggestions = [];
              for (const movie of result.suggestions) {
                currentSuggestions.push(new SearchMovie(movie));
              }
              resSuggestions.push(currentSuggestions);
              setLastRatedSuggestions(resSuggestions);
              setLastRatedMovies(resLastRatedMovies);
              setPendingReceive(false);
            } else {
              handleBadResponse();
            }
          }
        }).catch((_) => {
          handleBadResponse();
        });
    }

    const displayLastRatedSuggestionsMovies = (index) => {
        const movies = [];
        for (const movie of lastRatedSuggestions[index]) {
          const key = movie.movie_id + '_' + (index + OFFSET);
          movies.push(<MovieCard key={key} movie={movie}></MovieCard>);
        }
        return movies;
    };

    const displayAllLastRatedSuggestions = () => {
        const res = [];
        for (let i = 0; i < NUMBER_OF_SUGGESTIONS; i++) {
            if (lastRatedMovies.length <= i) {
                res.push(
                        <Mui.Box display="flex" key={'lastRatedSuggestions_' + OFFSET + '_' + i + '_LoadingText'} justifyContent="flex-start" mx={10} p={1}>
                            <Mui.Box p={1}>
                                <Mui.Typography variant="h4" gutterBottom>{t('suggestions.lastRated.text')} <Mui.CircularProgress />:</Mui.Typography>
                            </Mui.Box>
                        </Mui.Box>
                );
                res.push(
                    <LoadingCarousel key={'lastRatedSuggestions_' + OFFSET + '_' + i + '_LoadingCarousel'} cardKey={'lastRatedSuggestions_' + OFFSET + '_' + i} slidesToShow={5}></LoadingCarousel>
                )
            } else {
                if (lastRatedMovies[i] != null) {
                    const currentSuggestions = displayLastRatedSuggestionsMovies(i);
                    let nbOfSlidesToScroll = 5;
                    while (currentSuggestions.length % nbOfSlidesToScroll !== 0) {
                        nbOfSlidesToScroll--;
                    }
                    const settings = {
                        infinite: true,
                        slidesToShow: 5,
                        slidesToScroll: nbOfSlidesToScroll
                    };
                    res.push(
                        <Mui.Box key={'lastRatedSuggestions_' + OFFSET + '_' + i + '_text'} display="flex" justifyContent="flex-start" mx={10} p={1}>
                            <Mui.Box p={1}>
                                <Mui.Typography variant="h4" gutterBottom>{t('suggestions.lastRated.text')} <MovieLink href={'/Movie/' + lastRatedMovies[i].movie_id}>{lastRatedMovies[i].getTitle()}</MovieLink>:</Mui.Typography>
                            </Mui.Box>
                        </Mui.Box>
                    );
                    res.push(
                        <Mui.Box key={'lastRatedSuggestions_' + OFFSET + '_' + i + '_carousel'} display="block" mx={10} p={1}>
                            <HowToWatchCarousel {...settings}>
                                {currentSuggestions}
                            </HowToWatchCarousel>
                        </Mui.Box>
                    )
                }
            }
        }
        return res;
    }

    return (
        <>
            {displayAllLastRatedSuggestions()}
        </>
    );
}
