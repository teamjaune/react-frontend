import * as React from 'react';
import * as Mui from '@mui/material';
import { styled, useTheme } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';

export default function SearchUserResult(props) {
    const navigate = useNavigate();

    const theme = useTheme();

    const Card = styled(Mui.Card)(({ _ }) => ({
        'width': '80%',
        'marginBottom': '20px',
        'display': 'flex',
        'backgroundColor': theme.palette.background.movieResult,
        '.username': {
            'color': theme.palette.text.primary,
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'display': '-webkit-box',
            'WebkitLineClamp': '1',
            'WebkitBoxOrient': 'vertical'
        },
        '.hover:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    const show = () => {
        navigate('/User/' + props.user.id);
    };

    return (
        <Card>
            <Mui.CardMedia className='hover' sx={{ height: '200px', width: 'auto' }}
                component='img'
                onClick={show}
                src={process.env.REACT_APP_SERVER_ADDRESS + '/api/public/images/profile/' + props.user.id || '/images/icon.png'}
            />
            <Mui.Box sx={{ display: 'flex', flexDirection: 'column', height: '200px' }}>
                <Mui.CardContent>
                    <Mui.Typography className='username hover' align='left' gutterBottom variant='h5' component='div' onClick={show}>
                        {props.user.username}
                    </Mui.Typography>
                    {props.user.is_self && <Mui.Typography onClick={show} className='username' variant='h5'>(You)</Mui.Typography>}
                </Mui.CardContent>
            </Mui.Box>
        </Card >
    );
}
