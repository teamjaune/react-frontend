import * as React from 'react';
import * as Mui from '@mui/material';
import SearchMovieResult from './SearchMovieResult';

export default function SearchMoviesTab(props) {
    const displayMovies = () => {
        if (props.movies === undefined) {
            return <Mui.Typography variant='h5'>Server error.</Mui.Typography>;
        }
        if (props.loading && props.movies.length <= 0) {
            return <></>;
        }
        if (props.movies.length <= 0) {
            return <Mui.Typography variant='h5'>No results.</Mui.Typography>;
        }
        return props.movies.map(function (movie) {
            return <SearchMovieResult movie={movie} key={movie.movie_id}></SearchMovieResult>;
        });
    };

    return (
        <Mui.Stack
            direction="column"
            justifyContent="center"
            sx={{ width: '100%' }}
            alignItems="center"
        >
            {displayMovies()}
        </Mui.Stack>
    );
}
