import * as React from 'react';
import * as Mui from '@mui/material';
import { styled, useTheme } from '@mui/material/styles';
import RatingDisplay from '../RatingDisplay';
import { useNavigate } from 'react-router-dom';

export default function SearchMovieResult(props) {
    const navigate = useNavigate();

    const movie = props.movie;
    const ourRating = (movie.our_rating ?? 0) / 2;
    const userRating = (movie.user_rating ?? 0) / 2;
    const userRatingPresent = movie.user_rating !== undefined && movie.user_rating !== null;

    const theme = useTheme();

    const Card = styled(Mui.Card)(({ _ }) => ({
        'width': '80%',
        'marginBottom': '20px',
        'display': 'flex',
        'backgroundColor': theme.palette.background.movieResult,
        '.title': {
            'color': theme.palette.text.primary,
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'display': '-webkit-box',
            'WebkitLineClamp': '1',
            'WebkitBoxOrient': 'vertical'
        },
        '.synopsis': {
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'display': '-webkit-box',
            'WebkitLineClamp': '1',
            'WebkitBoxOrient': 'vertical'
        },
        '.hover:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    const show = () => {
        navigate('/Movie/' + movie.movie_id);
    };

    return (
        <Card>
            <Mui.CardMedia className='hover' sx={{ height: '200px', width: 'auto' }}
                component='img'
                onClick={show}
                image={movie.image_url}
            />
            <Mui.Box sx={{ display: 'flex', flexDirection: 'column', height: '200px', width: '100%' }}>
                <Mui.CardContent>
                    <Mui.Typography className='title hover' align='left' gutterBottom variant='h5' component='div' onClick={show}>
                        {movie.getTitle()}
                    </Mui.Typography>
                    <Mui.Typography align='left' gutterBottom variant='subtitle2' component='div' onClick={show}>
                        {movie.start_year}
                    </Mui.Typography>
                    <Mui.Stack direction="row"
                        justifyContent={userRatingPresent ? 'space-evenly' : 'flex-start'}
                        alignItems="center"
                    >
                        {userRatingPresent &&
                            <>
                                <RatingDisplay readOnly={true} color='#1976d2' title='User Rating' rating={userRating}></RatingDisplay>
                                <Mui.Divider orientation="vertical" flexItem />
                            </>
                        }
                        <RatingDisplay title={userRatingPresent ? 'My Rating' : ''} movie={movie} rating={ourRating}></RatingDisplay>
                    </Mui.Stack>
                    <Mui.Typography className='synopsis' align='left' gutterBottom variant='subtitle1' component='div' onClick={show}>
                        {movie.synopsis}
                    </Mui.Typography>
                </Mui.CardContent>
            </Mui.Box>
        </Card >
    );
}
