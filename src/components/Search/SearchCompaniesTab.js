import * as React from 'react';
import * as Mui from '@mui/material';
import SearchCompanyResult from './SearchCompanyResult';

export default function SearchCompaniesTab(props) {
    const displayCompanies = () => {
        if (props.loading && props.companies.length <= 0) {
            return <></>;
        }
        if (props.companies === undefined) {
            return <Mui.Typography variant='h5'>Server error.</Mui.Typography>;
        }
        if (props.companies.length <= 0) {
            return <Mui.Typography variant='h5'>No results.</Mui.Typography>;
        }
        return props.companies.map(function (company) {
            return <SearchCompanyResult company={company} key={company.company_id}></SearchCompanyResult>;
        });
    };

    return (
        <Mui.Stack
            direction="column"
            justifyContent="center"
            sx={{ width: '100%' }}
            alignItems="center"
        >
            {displayCompanies()}
        </Mui.Stack>
    );
}
