import * as React from 'react';
import * as Mui from '@mui/material';
import { styled, useTheme } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';

export default function SearchCompanyResult(props) {
    const navigate = useNavigate();

    const theme = useTheme();

    const Card = styled(Mui.Card)(({ _ }) => ({
        'width': '80%',
        'marginBottom': '20px',
        'display': 'flex',
        'backgroundColor': theme.palette.background.movieResult,
        '.name': {
            'color': theme.palette.text.primary,
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'display': '-webkit-box',
            'WebkitLineClamp': '1',
            'WebkitBoxOrient': 'vertical'
        },
        '.description': {
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'display': '-webkit-box',
            'WebkitLineClamp': '2',
            'WebkitBoxOrient': 'vertical'
        },
        '.hover:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    const show = () => {
        navigate('/Company/' + props.company.company_id);
    };

    return (
        <Card>
            <Mui.CardMedia className='hover' sx={{ backgroundColor: 'white', height: '200px', width: 'auto' }}
                component='img'
                onClick={show}
                image={props.company.image_url ? props.company.image_url : '../images/no_image_found.png'}
            />
            <Mui.Box sx={{ display: 'flex', flexDirection: 'column', height: '200px' }}>
                <Mui.CardContent>
                    <Mui.Typography className='name hover' align='left' gutterBottom variant='h5' component='div' onClick={show}>
                        {props.company.name}
                    </Mui.Typography>
                    <Mui.Typography align='left' gutterBottom variant='subtitle2' component='div' onClick={show}>
                        {props.company.origin_country}
                    </Mui.Typography>
                    <Mui.Typography className='description' align='left' gutterBottom variant='subtitle1' component='div' onClick={show}>
                        {props.company.description}
                    </Mui.Typography>
                </Mui.CardContent>
            </Mui.Box>
        </Card >
    );
}
