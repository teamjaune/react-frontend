import * as React from 'react';
import * as Mui from '@mui/material';
import SearchCrewResult from './SearchCrewResult';

export default function SearchCrewTab(props) {
    const displayCrewMembers = () => {
        if (props.crewMembers === undefined) {
            return <Mui.Typography variant='h5'>Server error.</Mui.Typography>;
        }
        if (props.loading && props.crewMembers.length <= 0) {
            return <></>;
        }
        if (props.crewMembers.length <= 0) {
            return <Mui.Typography variant='h5'>No results.</Mui.Typography>;
        }
        return props.crewMembers.map(function (crew) {
            return <SearchCrewResult crew={crew} key={crew.person_id}></SearchCrewResult>;
        });
    };

    return (
        <Mui.Stack
            direction="column"
            justifyContent="center"
            sx={{ width: '100%' }}
            alignItems="center"
        >
            {displayCrewMembers()}
        </Mui.Stack>
    );
}
