import * as React from 'react';
import * as Mui from '@mui/material';
import LoadingWaitingCard from '../Cards/LoadingWaitingCard';

export default function SearchLoadingTab(props) {
    const displayLoading = () => {
        const movies = [];
        for (let i = 0; i < 3; i++) {
            const key = props.tab + '_' + i;
            movies.push(<LoadingWaitingCard key={key} displayRating={props.displayRating}></LoadingWaitingCard>);
        }
        return movies;
    };

    return (
        <Mui.Stack
            direction="column"
            justifyContent="center"
            sx={{ width: '100%' }}
            alignItems="center"
        >
            {displayLoading()}
        </Mui.Stack>
    );
}
