import * as React from 'react';
import * as Mui from '@mui/material';
import SearchUserResult from './SearchUserResult';

export default function SearchUsersTab(props) {
    const displayUsers = () => {
        if (props.users === undefined) {
            return <Mui.Typography variant='h5'>Server error.</Mui.Typography>;
        }
        if (props.loading && props.users.length <= 0) {
            return <></>;
        }
        if (props.users.length <= 0) {
            return <Mui.Typography variant='h5'>No results.</Mui.Typography>;
        }
        return props.users.map(function (user) {
            return <SearchUserResult user={user} key={user.id}></SearchUserResult>;
        });
    };

    return (
        <Mui.Stack
            direction="column"
            justifyContent="center"
            sx={{ width: '100%' }}
            alignItems="center"
        >
            {displayUsers()}
        </Mui.Stack>
    );
}
