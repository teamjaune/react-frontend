import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from 'react-slick';
import { styled } from '@mui/material/styles';

export default styled(Slider)(({ _ }) => ({
    'display': 'flex',

    '.slick-track': {
        'display': 'flex !important'
    },

    '.slick-slide': {
        'height': 'inherit !important'
    },

    '.slick-arrow': {
        'transform': 'none',
        'background': '#DCDCDC',
        'position': 'relative',
        'left': '0',
        'right': '0',
        'height': 'auto'
    },
    '.slick-arrow:hover': {
        'background': '#DCDCDC'
    },
    '.slick-arrow:before': {
        'color': 'black'
    }
}));
