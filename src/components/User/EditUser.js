import * as React from 'react';
import * as Mui from '@mui/material';
import * as MuiIcons from '@mui/icons-material';
import { getAuth } from 'firebase/auth';
import Communication from '../../service/communication';
import { useTranslation } from 'react-i18next';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Mui.Slide direction="up" ref={ref} {...props} />;
});

export default function EditUser({ fetchProfile, user, createOpenPopupFunction }) {
    const [error, setError] = React.useState(false);

    const [open, setOpen] = React.useState(false);

    const [username, setUsername] = React.useState(user ? user.username : '');
    const [file, setFile] = React.useState(user ? user.picture : '');

    const [t] = useTranslation('common');

    React.useEffect(() => {
        function openPopupFunction() {
            handleClickOpen();
        }
        createOpenPopupFunction(() => openPopupFunction);
        setUsername(user ? user.username : '');
        setFile(user ? user.picture : '');
    }, [createOpenPopupFunction, user]);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        fetchProfile();
        setOpen(false);
    };

    const save = () => {
        setError(false);
        getAuth().currentUser.getIdToken().then(token => {
            const formdata = new FormData();
            if (username && username.length > 0) {
                formdata.append('username', username);
            }
            formdata.append('file', file);
            Communication.sendFormPostRequest('api/private/user', token, formdata).then(async (response) => {
                if (response.status === 200) {
                    handleClose();
                } else {
                    setError(true);
                }
            }).catch((_) => {
                setError(true);
            });
        });
    };

    const isFileImage = (file) => {
        return file && file.type.split('/')[0] === 'image';
    };

    const selectFile = (file) => {
        if (isFileImage(file)) {
            setFile(file);
        }
    };

    return (
        <>
            <Mui.Dialog
                fullScreen
                open={open}
                onClose={handleClose}
                TransitionComponent={Transition}
            >
                <Mui.AppBar sx={{ position: 'relative' }}>
                    <Mui.Toolbar>
                        <Mui.IconButton
                            edge="start"
                            color="inherit"
                            onClick={handleClose}
                            aria-label="close"
                        >
                            <MuiIcons.Close />
                        </Mui.IconButton>
                        <Mui.Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                            {t('user.editProfile')}
                        </Mui.Typography>
                        <Mui.Button variant='outlined' color="inherit" onClick={save}>
                            {t('user.save')}
                        </Mui.Button>
                    </Mui.Toolbar>
                </Mui.AppBar>
                <Mui.List>
                    <Mui.ListItem>
                        {error && <Mui.Typography color='#FF0000'>Server error.</Mui.Typography>}
                    </Mui.ListItem>
                    <Mui.ListItem>
                        <input
                            type="file"
                            onChange={(e) => selectFile(e.target.files[0])}
                        />
                    </Mui.ListItem>
                    <Mui.ListItem>
                        <Mui.TextField
                            margin='normal'
                            error={error}
                            fullWidth
                            id='username'
                            label={t('user.username')}
                            name='username'
                            type='text'
                            value={username}
                            onChange={(event) => setUsername(event.target.value)}
                        />
                    </Mui.ListItem>
                </Mui.List>
            </Mui.Dialog>
        </>
    );
}
