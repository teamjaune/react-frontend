import * as React from 'react';
import * as Mui from '@mui/material';
import { styled } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';

export default function UserCard(props) {
    const navigate = useNavigate();

    const Card = styled(Mui.Card)(({ _ }) => ({
        '&:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    const show = () => {
        navigate('/Movie/' + props.movie.movie_id);
    };

    return (
        <Card sx={{ maxWidth: 250 }}>
            <Mui.CardContent>
                <Mui.Stack direction="row"
                    justifyContent="space-evenly"
                    alignItems="center"
                >
                    <Mui.Avatar src={props.user.picture} onClick={show} />
                </Mui.Stack>
                <Mui.Typography align="center" gutterBottom variant="h5" component="div" onClick={show}>
                    {props.user.username}
                </Mui.Typography>
            </Mui.CardContent>
            <Mui.CardActions>
            </Mui.CardActions>
        </Card>
    );
}
