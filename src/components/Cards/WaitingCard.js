import * as React from 'react';
import * as Mui from '@mui/material';
import { styled, useTheme } from '@mui/material/styles';

export default function WaitingCard() {
    const theme = useTheme();

    const Card = styled(Mui.Card)(({ _ }) => ({
        'height': '100%',
        'marginLeft': '5px',
        'marginRight': '5px',
        'display': 'flex',
        'justifyContent': 'space-between',
        'flexDirection': 'column',
        'backgroundColor': theme.palette.background.movieResult,
        '&:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    return (
        <Card>
            <a style={{ color: theme.palette.text.primary, textDecoration: 'none' }}>
                <Mui.CardMedia
                    component="img"
                    image='/images/waiting.png'
                />
                <Mui.CardContent>
                    <Mui.Typography className='title' align="center" gutterBottom variant="h6" component="div">
                        <Mui.CircularProgress />
                    </Mui.Typography>
                </Mui.CardContent>
            </a>
        </Card>
    );
}
