import * as React from 'react';
import * as Mui from '@mui/material';
import { styled, useTheme } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';

export default function MovieCard(props) {
    const navigate = useNavigate();

    const theme = useTheme();

    const Card = styled(Mui.Card)(({ _ }) => ({
        'height': '100%',
        'marginLeft': '5px',
        'marginRight': '5px',
        'display': 'flex',
        'justifyContent': 'end',
        'flexDirection': 'column',
        'backgroundColor': theme.palette.background.movieResult,
        '&:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        },
        '.title': {
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'display': '-webkit-box',
            'WebkitLineClamp': '1',
            'WebkitBoxOrient': 'vertical'
        }
    }));

    const show = () => {
        navigate('/Movie/' + props.movie.movie_id);
    };

    return (
        <Card>
            <Mui.Box sx={{ bgcolor: 'text.primary' }}>
                <Mui.CardMedia
                    onClick={show}
                    component="img"
                    image={props.movie.image_url}
                />
            </Mui.Box>
            <Mui.CardContent>
                <Mui.Typography className='title' align="center" gutterBottom variant="h6" component="div" onClick={show}>
                    {props.movie.getTitle()}
                </Mui.Typography>
                <Mui.Typography align="center" gutterBottom variant="h6" component="div" onClick={show}>
                    ({props.movie.start_year})
                </Mui.Typography>
            </Mui.CardContent>
            <Mui.CardActions>
                {props.movie.categories &&
                    <Mui.Stack direction="column"
                        justifyContent="space-evenly"
                        sx={{ width: '100%' }}
                        alignItems="center"
                    >
                        {props.movie.categories.map(category => (category != null &&
                            <Mui.Typography key={category} variant='h5' onClick={show}>
                                {category}
                            </Mui.Typography>
                        ))}
                    </Mui.Stack>}
            </Mui.CardActions>
        </Card>
    );
}
