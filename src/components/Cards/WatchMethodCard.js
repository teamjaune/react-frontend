import * as React from 'react';
import * as Mui from '@mui/material';
import { styled, useTheme } from '@mui/material/styles';

export default function WatchMethodCard(props) {
    const theme = useTheme();

    const Card = styled(Mui.Card)(({ _ }) => ({
        'width': '75px',
        'height': '100%',
        'marginLeft': '5px',
        'marginRight': '5px',
        'display': 'flex',
        'justifyContent': 'space-between',
        'flexDirection': 'column',
        'backgroundColor': theme.palette.background.movieResult,
        '.content': {
            'height': '100%'
        },
        '.css-46bh2p-MuiCardContent-root': {
            'padding': '0'
        },
        'name': {
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'display': '-webkit-box',
            'WebkitLineClamp': '1',
            'WebkitBoxOrient': 'vertical'
        }
    }));

    return (
        <Card>
            <Mui.CardMedia
                component='img'
                style={{ width: '75px', height: '75px' }}
                image={props.watchMethod.image_url ? props.watchMethod.image_url : '../images/no_image_found.png'}
            />
        </Card>
    );
}
