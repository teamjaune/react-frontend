import * as React from 'react';
import * as Mui from '@mui/material';
import { styled, useTheme } from '@mui/material/styles';
import RatingDisplay from '../RatingDisplay';

export default function LoadingWaitingCard(props) {
    const theme = useTheme();

    const Card = styled(Mui.Card)(({ _ }) => ({
        'width': '80%',
        'marginBottom': '20px',
        'display': 'flex',
        'backgroundColor': theme.palette.background.movieResult,
        '.title': {
            'color': theme.palette.text.primary,
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'display': '-webkit-box',
            'WebkitLineClamp': '1',
            'WebkitBoxOrient': 'vertical'
        },
        '.synopsis': {
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'display': '-webkit-box',
            'WebkitLineClamp': '1',
            'WebkitBoxOrient': 'vertical'
        },
        '.hover:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    return (
        <Card>
            <Mui.Link style={{ color: theme.palette.text.primary, textDecoration: 'none' }}>
                <Mui.CardMedia className='hover' sx={{ height: '200px', width: 'auto' }}
                    component="img"
                    image='/images/waiting.png'
                />
            </Mui.Link>
            <Mui.Box sx={{ display: 'flex', flexDirection: 'column', height: '200px' }}>
                <Mui.CardContent>
                    <Mui.Link style={{ color: theme.palette.text.primary, textDecoration: 'none' }}>
                        <Mui.Typography className='title hover' align='left' gutterBottom variant='h5' component='div'>
                            <Mui.CircularProgress />
                        </Mui.Typography>
                    </Mui.Link>
                    <Mui.Typography align='left' gutterBottom variant='subtitle2' component='div'>
                        <Mui.CircularProgress />
                    </Mui.Typography>
                    <Mui.Stack direction="row"
                        justifyContent={'flex-start'}
                        alignItems="center"
                    >
                    {props.displayRating && <RatingDisplay title={''} movie={null} readOnly={true} rating={5}></RatingDisplay>}
                    </Mui.Stack>
                    <Mui.Typography className='synopsis' align='left' gutterBottom variant='subtitle1' component='div'>
                        <Mui.CircularProgress />
                    </Mui.Typography>
                </Mui.CardContent>
            </Mui.Box>
        </Card >
    );
}
