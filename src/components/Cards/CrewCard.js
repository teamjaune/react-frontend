import * as React from 'react';
import * as Mui from '@mui/material';
import { styled, useTheme } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';

export default function CrewCard(props) {
    const navigate = useNavigate();

    const theme = useTheme();

    const Card = styled(Mui.Card)(({ _ }) => ({
        'height': '100%',
        'marginLeft': '5px',
        'marginRight': '5px',
        'display': 'flex',
        'justifyContent': 'space-between',
        'flexDirection': 'column',
        'backgroundColor': theme.palette.background.movieResult,
        '&:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        },
        '.MuiCardContent-root': {
            'padding': '16px 0'
        },
        '.crewName': {
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'display': '-webkit-box',
            'WebkitLineClamp': '1',
            'WebkitBoxOrient': 'vertical'
        }
    }));

    const show = () => {
        navigate('/Crew/' + props.crew.person_id);
    };

    return (
        <Card>
            <Mui.CardMedia
                onClick={show}
                component="img"
                image={props.crew.image_url ? props.crew.image_url : '../images/no_image_found.png'}
            />
            <Mui.CardContent>
                <Mui.Typography className='crewName' align="center" gutterBottom variant="h6" component="div" onClick={show}>
                    {props.crew.name}
                </Mui.Typography>
            </Mui.CardContent>
            <Mui.CardActions>
                {props.crew.categories &&
                    <Mui.Stack direction="row"
                        justifyContent="center"
                        sx={{ width: '100%', overflow: 'hidden' }}
                        alignItems="center"
                    >
                        {props.crew.categories.map(category => (
                            <Mui.Typography
                                style={{ margin: '3px' }}
                                key={category}
                                variant='h5'
                                onClick={show}
                            >
                                {category}
                            </Mui.Typography>
                        ))}
                    </Mui.Stack>}
            </Mui.CardActions>
        </Card>
    );
}
