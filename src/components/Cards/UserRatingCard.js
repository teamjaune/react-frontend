import * as React from 'react';
import * as Mui from '@mui/material';
import { styled, useTheme } from '@mui/material/styles';
import RatingDisplay from '../RatingDisplay';

export default function UserRatingCard(props) {
    const rating = props.userRating.rating / 2;
    const theme = useTheme();

    const Card = styled(Mui.Card)(({ _ }) => ({
        'backgroundColor': theme.palette.background.movieResult,
        '&:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    return (
        <Card sx={{ maxWidth: 250 }}>
            <a style={{ color: theme.palette.text.primary, textDecoration: 'none' }} href={'/User/' + props.userRating.id}>
                <Mui.CardContent>
                    <Mui.Stack direction="row"
                        justifyContent="space-evenly"
                        alignItems="center"
                    >
                        <Mui.Avatar src={props.userRating.picture} />
                    </Mui.Stack>
                    <Mui.Typography align="center" gutterBottom variant="h5" component="div">
                        {props.userRating.username}
                    </Mui.Typography>
                    <RatingDisplay readOnly={true} color='#1976d2' rating={rating}></RatingDisplay>
                </Mui.CardContent>
                <Mui.CardActions>
                </Mui.CardActions>
            </a>
        </Card>
    );
}
