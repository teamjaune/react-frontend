import * as React from 'react'
import * as Mui from '@mui/material'
import * as MuiIcons from '@mui/icons-material';
import { useTranslation } from 'react-i18next';

export default function WelcomeMessage(signIn = true) {
    const [t] = useTranslation('common');

    return (
        <Mui.Box
            sx={{
                width: '100%',
                height: '100%',
                backgroundColor: 'black',
                opacity: [0.8]
            }}
        >
            <Mui.Grid
                container
                alignContent='center'
                sx={{
                    display: 'flex',
                    flexDirection: 'column'
                }}
            >
                <Mui.Grid item
                    sx={{
                        p: 4,
                        m: 4,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center'
                    }}
                >
                    <Mui.Typography variant='h2' color='white' textAlign='center'>
                        {signIn ? t('login.welcomeBack') : t('login.welcomeNewcomer')}
                    </Mui.Typography>
                    <Mui.Typography
                        variant='h4'
                        color='white'
                        letterSpacing='-1px'
                        wordSpacing='-0.2px'
                        fontWeight='700'
                        fontStyle='bold'
                        fontVariant='small-caps'
                        textAlign='center'
                    >
                        {signIn ? t('login.seeYouAgain') : t('login.wantToJoin')}
                    </Mui.Typography>
                </Mui.Grid>
                <Mui.Grid
                    sx={{
                        px: 4,
                        paddingBottom: 4,
                        mx: 4,
                        display: 'flex',
                        flexDirection: 'column'
                    }}
                >
                    <Mui.Typography fontSize='x-large' variant='body1' color='white' sx={{ my: 0.5 }} position='relative' boxSizing='content-box'>
                        {signIn ? t('login.signInNow') : t('login.signUpNow')}
                    </Mui.Typography>
                    <Mui.Typography fontSize='large' variant='body1' color='white' sx={{ my: 0.5 }} position='relative' boxSizing='content-box'>
                        <MuiIcons.StarBorder fontSize='large' color='primary' sx={{ mx: 1 }} />
                        <strong>{t('login.rate')}</strong> {t('login.yourFavorite')}
                    </Mui.Typography>
                    <Mui.Typography fontSize='large' variant='body1' color='white' sx={{ my: 0.5 }} position='relative' boxSizing='content-box'>
                        <MuiIcons.AutoFixHigh fontSize='large' color='primary' sx={{ mx: 1 }} />
                        {t('login.letOur')} <strong>{t('login.AI')}</strong> {t('login.choose')}
                    </Mui.Typography>
                    <Mui.Typography fontSize='large' variant='body1' color='white' sx={{ my: 0.5 }} position='relative' boxSizing='content-box'>
                        <MuiIcons.RecommendOutlined fontSize='large' color='primary' sx={{ mx: 1 }} />
                        {t('login.discover')} <strong>{t('login.appreciated')}</strong> {t('login.moviesOfAllTime')}
                    </Mui.Typography>
                    <Mui.Typography fontSize='large' variant='body1' color='white' sx={{ my: 0.5 }} position='relative' boxSizing='content-box'>
                        <MuiIcons.GroupsOutlined fontSize='large' color='primary' sx={{ mx: 1 }} />
                        {t('login.find')} <strong>{t('login.watchTogether')}</strong> {t('login.withFriends')}
                    </Mui.Typography>
                </Mui.Grid>
            </Mui.Grid>
        </Mui.Box>
    )
}
