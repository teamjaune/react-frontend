/* eslint-disable no-undef */
export const ErrorType = {
    NO_ERROR: 'NO_ERROR',
    EMAIL: 'EMAIL',
    PASSWORD: 'PASSWORD',
    FIREBASE: 'FIREBASE',
    UNKNOWN: 'UNKNOWN',
    POPUP: 'POPUP',
    SUCESS: 'SUCESS'
}

/* eslint-enable no-undef */
export const ErrorMessages = {
    NO_ERROR: { message: '', type: ErrorType.NO_ERROR },
    INVALID_EMAIL: { message: 'Invalid email. Please provide a valid email address.', type: ErrorType.EMAIL },
    SIGN_IN: { message: 'Incorrect email or password. Please check the data provided.', type: ErrorType.EMAIL },
    USER_EXISTS: { message: 'Email already used! Please choose another address.', type: ErrorType.EMAIL },
    PASSWORD_NOT_SECURE: { message: 'Password not secure. Please use at least 8 characters including numbers and letters.', type: ErrorType.PASSWORD },
    PASSWORD_DO_NOT_MATCH: { message: 'Passwords do not match. Please re-enter your password in the confirmation password field.', type: ErrorType.PASSWORD },
    FIREBASE_ERROR: { message: 'An error occurred. Please check the data provided and try again. If the error persists, contact support.', type: ErrorType.FIREBASE },
    UNKNOWN_ERROR: { message: 'An error occurred during registration. Please try again. If the error persists, contact support.', type: ErrorType.UNKNOWN },
    POPUP_CLOSE: { message: 'Please do not close popups in order to use Google\'s services.', type: ErrorType.POPUP },
    POPUP_BLOCK: { message: 'Please allow popups in order to use Google\'s services.', type: ErrorType.POPUP },
    WRONG_PASSWORD: { message: 'Incorrect password. Please try again.', type: ErrorType.PASSWORD },
    PASSWORD_NULL: { message: 'Password empty. Please enter a valid password', type: ErrorType.PASSWORD },
    RESET_PASS_SUCCESS: { message: 'We have sent an email to the address you provided with a link to reset your password.', type: ErrorType.SUCESS },
    USER_NOT_FOUND: { message: 'Email not found. Please verify the email address provided.', type: ErrorType.EMAIL }
}
