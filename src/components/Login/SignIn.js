import * as React from 'react';
import * as Mui from '@mui/material';
import { getAuth, signInWithEmailAndPassword, setPersistence, browserSessionPersistence, indexedDBLocalPersistence, sendPasswordResetEmail } from 'firebase/auth';
import * as MuiIcons from '@mui/icons-material';
import { ErrorMessages, ErrorType } from './ErrorMessage';
import { useTranslation } from 'react-i18next';

export default function SignIn() {
    const [emailField, setEmailField] = React.useState('');
    const [emailFieldRestPassword, setEmailFieldRestPassword] = React.useState('');
    const [passwordField, setPasswordField] = React.useState('');
    const [errorMessage, setErrorMessage] = React.useState(ErrorMessages.NO_ERROR);
    const [passwordShown, setPasswordShown] = React.useState(false); // Use to show or not the password
    const [rememberMe, setRememberMe] = React.useState(false);
    const [open, setOpen] = React.useState(false);
    const [openAlert, setOpenAlert] = React.useState(false);
    const [messageResetPassword, setMessageResetPassword] = React.useState(ErrorMessages.NO_ERROR);

    const [t] = useTranslation('common');

    const auth = getAuth();

    const togglePasswordVisiblity = () => {
        setPasswordShown(!passwordShown);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
    };

    const handleChangeRememberMe = () => {
        setRememberMe(!rememberMe);
        setPersistence(auth, rememberMe ? browserSessionPersistence : indexedDBLocalPersistence)
            .catch(error => {
                if (error.code != null) {
                    setErrorMessage(ErrorMessages.UNKNOWN_ERROR);
                }
            });
    };

    const signInFirebaseEmail = () => {
        signInWithEmailAndPassword(auth, emailField, passwordField)
            .catch(error => {
                if (error.code != null) {
                    setErrorMessage(ErrorMessages.SIGN_IN);
                }
            });
    };

    const keyDown = (event) => {
        if (event.key === 'Enter') {
            signInFirebaseEmail();
        }
    };

    const handleClose = () => {
        setOpen(false);
        setOpenAlert(false);
        setMessageResetPassword(ErrorMessages.NO_ERROR);
        setEmailFieldRestPassword('');
    };

    const passwordReset = async () => {
        await sendPasswordResetEmail(auth, emailFieldRestPassword)
        .then(() => {
            setMessageResetPassword(ErrorMessages.RESET_PASS_SUCCESS);
        })
        .catch((error) => {
            if (error.code === 'auth/invalid-email') setMessageResetPassword(ErrorMessages.INVALID_EMAIL);
            else if (error.code === 'auth/user-not-found') setMessageResetPassword(ErrorMessages.USER_NOT_FOUND);
            else setMessageResetPassword(ErrorMessages.FIREBASE_ERROR);
        });
        setOpenAlert(true);
    };

    const messageCard = () => {
        return (
            <Mui.Box sx={{ py: 1 }}>
                <Mui.Alert
                    severity={messageResetPassword.type !== ErrorType.SUCESS ? 'error' : 'success'}
                    action={
                    <Mui.IconButton
                        aria-label='close'
                        color='inherit'
                        size='small'
                        onClick={() => {
                            setOpenAlert(false);
                        }}
                    >
                        <MuiIcons.Close fontSize='inherit' />
                    </Mui.IconButton>
                    }
                >
                    {messageResetPassword.message}
                </Mui.Alert>
            </Mui.Box>
        );
    }

    return (
        <Mui.Box style={{ width: '60%' }}>
            {errorMessage.type !== ErrorType.NO_ERROR &&
                <Mui.Box sx={{ py: 1 }}>
                    <Mui.Alert
                        severity='error'
                        action={
                            <Mui.IconButton
                                aria-label='close'
                                color='inherit'
                                size='small'
                                onClick={() => {
                                    setErrorMessage(ErrorMessages.NO_ERROR);
                                }}
                            >
                                <MuiIcons.Close fontSize='inherit' />
                            </Mui.IconButton>
                        }
                    >
                        <strong>Error:</strong> {errorMessage.message}
                    </Mui.Alert>
                </Mui.Box>
            }
            <Mui.Box component='form' noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
                <Mui.Grid container spacing={2}>
                    <Mui.Grid item xs={12}>
                        <Mui.TextField
                            error={errorMessage.type !== ErrorType.NO_ERROR && emailField.length === 0}
                            margin='normal'
                            required
                            fullWidth
                            id='email'
                            label={t('login.emailAdress')}
                            name='email'
                            type='email'
                            autoComplete='current-email'
                            autoFocus
                            value={emailField}
                            onChange={(event) => setEmailField(event.target.value)}
                            onKeyDown={(event) => keyDown(event)}
                            InputProps={{
                                style: {
                                    padding: 0
                                }
                            }}
                        />
                    </Mui.Grid>
                    <Mui.Grid item xs={12}>
                        <Mui.TextField
                            sx={{ p: 0 }}
                            error={errorMessage.type !== ErrorType.NO_ERROR && passwordField.length === 0}
                            margin='normal'
                            required
                            fullWidth
                            name='password'
                            label={t('login.password')}
                            type={passwordShown ? 'text' : 'password'}
                            id='password'
                            autoComplete='current-password'
                            value={passwordField}
                            onChange={(event) => setPasswordField(event.target.value)}
                            onKeyDown={(event) => keyDown(event)}
                            InputProps={{
                                style: {
                                    padding: 0
                                },
                                endAdornment: (
                                    <Mui.IconButton onClick={togglePasswordVisiblity} >
                                        <Mui.Tooltip title={passwordShown ? t('login.hide') : t('login.shown')}>
                                            {passwordShown
                                                ? <MuiIcons.Visibility color='primary' sx={{ mx: 1 }} />
                                                : <MuiIcons.VisibilityOff color='primary' sx={{ mx: 1 }} />}
                                        </Mui.Tooltip>
                                    </Mui.IconButton >
                                )
                            }}
                        />
                    </Mui.Grid>
                    <Mui.Grid item xs={12}>
                        <Mui.FormControlLabel
                            control={<Mui.Checkbox value={rememberMe} onChange={handleChangeRememberMe} color='primary' />}
                            label={t('login.rememberMe')}
                        />
                    </Mui.Grid>
                </Mui.Grid>
                <Mui.Button
                    type='submit'
                    fullWidth
                    variant='contained'
                    sx={{ mt: 3, mb: 2 }}
                    onClick={() => signInFirebaseEmail()}
                >
                    {t('login.signIn')}
                </Mui.Button>
                <Mui.Grid>
                    <Mui.Button variant='text' onClick={() => { setOpen(true); }} >
                        {t('login.forgotPassword')}
                    </Mui.Button>
                </Mui.Grid>
            </Mui.Box>

            <Mui.Dialog open={open} onClose={handleClose}>
                <Mui.DialogTitle>{t('login.resetPassword')}</Mui.DialogTitle>
                <Mui.DialogContent>
                    {openAlert && messageCard()}
                    <Mui.DialogContentText>
                        {t('login.enterEmail')}
                    </Mui.DialogContentText>
                    <Mui.TextField
                        margin='normal'
                        required
                        fullWidth
                        id='email'
                        label={t('login.emailAdress')}
                        name='email'
                        type='email'
                        autoComplete='email'
                        autoFocus
                        value={emailFieldRestPassword}
                        onChange={(event) => setEmailFieldRestPassword(event.target.value)}
                        onKeyDown={(event) => keyDown(event)}
                    />
                </Mui.DialogContent>
                <Mui.DialogActions>
                    <Mui.Button onClick={handleClose}>{t('login.close')}</Mui.Button>
                    {messageResetPassword.type !== ErrorType.SUCESS && <Mui.Button onClick={passwordReset}>{t('login.submit')}</Mui.Button>}
                </Mui.DialogActions>
            </Mui.Dialog>
        </Mui.Box>
    );
}
