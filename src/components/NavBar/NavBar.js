import * as React from 'react';
import * as Mui from '@mui/material';
import * as MuiIcons from '@mui/icons-material';
import NavBarProfileButton from './NavBarProfileButton';
import NavBarLanguageButton from './NavBarLanguageButton';
import NavBarSearchField from './NavBarSearchField';
import { useNavigate } from 'react-router-dom';
import { styled, useTheme } from '@mui/material/styles';

import IconButton from '@mui/material/IconButton';

export default function NavBar(props) {
    const navigate = useNavigate();

    const Title = styled(Mui.Typography)(({ _ }) => ({
        '&:hover': {
            'cursor': 'pointer'
        },
        '.icon': {
            'width': '35px',
            'height': '35px',
            'marginRight': '15px',
            '&:hover': {
                'cursor': 'pointer'
            }
        }
    }));

    const theme = useTheme();
    return (
        <Mui.AppBar position="static">
            <Mui.Stack direction="row"
                justifyContent="space-evenly"
                alignItems="center">
                <Title variant="h6" component="div" onClick={() => navigate('/')}>
                    <img className='icon' onClick={() => navigate('/')} src='/images/icon.png' />
                    Movie Suggester
                </Title>
                <NavBarSearchField />
                <Mui.Stack direction="row"
                    justifyContent="center"
                    alignItems="center">
                    <Mui.Link href='/Suggestions'>
                        <Mui.IconButton>
                            <MuiIcons.Movie sx={{ color: '#FFFFFF' }} />
                        </Mui.IconButton>
                    </Mui.Link>
                    <IconButton sx={{ ml: 1 }} onClick={props.toggleColorMode} color="inherit">
                        {theme.palette.mode === 'dark' ? <MuiIcons.Brightness7 /> : <MuiIcons.Brightness4 />}
                    </IconButton>
                    <NavBarLanguageButton></NavBarLanguageButton>
                    <NavBarProfileButton></NavBarProfileButton>
                    <IconButton sx={{ ml: 1 }} onClick={props.toggleColorMode} color="inherit">
                        {theme.palette.mode === 'dark' ? <MuiIcons.Brightness7 /> : <MuiIcons.Brightness4 />}
                    </IconButton>
                </Mui.Stack>
            </Mui.Stack>
        </Mui.AppBar>
    );
}
