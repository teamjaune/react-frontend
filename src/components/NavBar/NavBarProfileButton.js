import * as React from 'react';
import * as Mui from '@mui/material';
import * as MuiIcons from '@mui/icons-material';
import { getAuth } from 'firebase/auth';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export default function NavBarProfileButton() {
    const navigate = useNavigate();

    const [anchorEl, setAnchorEl] = React.useState(null);

    const [t] = useTranslation('common');

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const showProfile = () => {
        navigate('/User/');
        handleClose();
    };

    const showSettings = () => {
        navigate('/Settings');
        handleClose();
    };

    const showCredit = () => {
        navigate('/Credits');
        handleClose();
    };

    const logOut = () => {
        getAuth().signOut();
    };

    return (
        <>
            <Mui.IconButton
                size='large'
                aria-label='account of current user'
                aria-controls='menu-appbar'
                aria-haspopup='true'
                onClick={handleMenu}
                color='inherit'
            >
                <MuiIcons.AccountCircle />
            </Mui.IconButton>
            <Mui.Menu
                id='menu-appbar'
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                }}
                sx={{ my: 4 }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                }}
                open={anchorEl !== null}
                onClose={handleClose}
            >
                <Mui.MenuItem onClick={showProfile}>
                    <MuiIcons.AccountBox />
                    {t('navBarProfileButton.profile')}
                </Mui.MenuItem>
                <Mui.MenuItem onClick={showSettings}>
                    <MuiIcons.Settings />
                    {t('navBarProfileButton.settings')}
                </Mui.MenuItem>
                <Mui.MenuItem onClick={showCredit}>
                    <MuiIcons.Info />
                    {t('navBarProfileButton.credits')}
                </Mui.MenuItem>
                <Mui.Divider sx={{ my: 0.5 }} />
                <Mui.MenuItem onClick={logOut}>
                    <MuiIcons.Logout />
                    {t('navBarProfileButton.logOut')}
                </Mui.MenuItem>
            </Mui.Menu>
        </>
    );
}
