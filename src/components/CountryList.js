import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import ReactCountryFlag from 'react-country-flag';

export default function CountryList(props) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [selectedIndex, setSelectedIndex] = React.useState(props.countryList.findIndex(elem => elem.country === props.currentCountry));
    const open = Boolean(anchorEl);

    const handleClickListItem = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const selectNewCountryIndex = (index) => {
        setSelectedIndex(index);
        props.countrySelected(props.countryList[index].country);
    }

    const handleMenuItemClick = (event, index) => {
        selectNewCountryIndex(index);
        setAnchorEl(null);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const displayCurrentValue = () => {
        if (selectedIndex < 0) {
            return;
        }
        return (
            <>
                <ReactCountryFlag style={{ marginRight: '5px' }} countryCode={props.countryList[selectedIndex].country} svg /> {props.countryList[selectedIndex].country}
            </>
        );
    };

    if (selectedIndex < 0) {
        selectNewCountryIndex(0);
    }

    return (
        <>
            <div>
                <List
                    component="nav"
                    aria-label="Device settings"
                    sx={{ bgcolor: 'background.paper' }}
                >
                    <ListItem
                    button
                    id="lock-button"
                    aria-haspopup="listbox"
                    aria-controls="lock-menu"
                    aria-label="when device is locked"
                    aria-expanded={open ? 'true' : undefined}
                    onClick={handleClickListItem}
                    >
                    <ListItemText
                        primary= {displayCurrentValue()}
                    />
                    </ListItem>
                </List>
                <Menu
                    id="lock-menu"
                    anchorEl={anchorEl}
                    open={open}
                    onClose={handleClose}
                    MenuListProps={{
                    'aria-labelledby': 'lock-button',
                    'role': 'listbox'
                    }}
                >
                    {props.countryList.map((option, index) => (
                    <MenuItem
                        key={option.country}
                        selected={index === selectedIndex}
                        onClick={(event) => handleMenuItemClick(event, index)}
                    >
                        <ReactCountryFlag style={{ marginRight: '5px' }} countryCode={option.country} svg /> {option.country}
                    </MenuItem>
                    ))}
                </Menu>
            </div>
        </>
    );
}
