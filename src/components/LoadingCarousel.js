import * as React from 'react';
import * as Mui from '@mui/material';
import WaitingCard from '../components/Cards/WaitingCard';
import HowToWatchCarousel from '../components/HowToWatchCarousel';

export default function LoadingCarousel(props) {
    const displayLoadingCards = () => {
        const movies = [];
        for (let i = 0; i < 24; i++) {
            const key = props.cardKey + '_' + i;
            movies.push(<WaitingCard key={key}></WaitingCard>);
        }
        return movies;
    };

    const settings = {
        slidesToShow: props.slidesToShow,
        slidesToScroll: props.slidesToShow
    };

    return (
        <>
            <Mui.Box display="block" mx={10} p={1}>
                <HowToWatchCarousel {...settings}>
                    {displayLoadingCards()}
                </HowToWatchCarousel>
            </Mui.Box>
        </>
    );
}
