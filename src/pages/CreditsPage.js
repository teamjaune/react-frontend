import * as React from 'react';
import * as Mui from '@mui/material';
import { useTranslation } from 'react-i18next';

export default function CreditsPage() {
    const [t] = useTranslation('common');

    return (
        <Mui.Box
            sx={{
                my: 8,
                mx: 6,
                height: '80%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center'
            }}
        >
            <Mui.Typography my={2} variant='h2' color='text.secondary' align='center' style={{ marginTop: 'auto' }}>
                {t('credits.credits')}
            </Mui.Typography>

            <Mui.Typography my={2} variant='body2' color='text.secondary' align='center' style={{ marginTop: 'auto' }}>
                {t('credits.mastersDegree')}
            </Mui.Typography>
            <Mui.Typography my={2} variant='body2' color='text.secondary' align='center' style={{ marginTop: 'auto' }}>
                {t('credits.tutoredProject')}
            </Mui.Typography>
            <Mui.Typography my={2} variant='body2' color='text.secondary' align='center' style={{ marginTop: 'auto' }}>
                {t('credits.supervisors')}
            </Mui.Typography>
            <Mui.Typography my={2} variant='body2' color='text.secondary' align='center' style={{ marginTop: 'auto' }}>
                {t('credits.pam')}
            </Mui.Typography>
            <Mui.Typography my={2} variant='body2' color='text.secondary' align='center' style={{ marginTop: 'auto' }}>
                {t('credits.designed')}
            </Mui.Typography>
            <Mui.Box my={2}>
                <Mui.Button
                    variant='contained'
                    size='small'
                    color='primary'
                    rel='noopener noreferrer'
                    href={'mailto:teamjaune@xyz.com'}
                >
                    <Mui.Typography variant='button' style={{ fontSize: '0.69rem' }}>
                        {t('credits.contactUs')}
                    </Mui.Typography>
                </Mui.Button>
            </Mui.Box>

            <Mui.Typography my={4} variant='body2' color='text.secondary' align='center' style={{ marginTop: 'auto' }}>
                Copyright © TeamJaune {new Date().getFullYear()}.
            </Mui.Typography>
        </Mui.Box>
    );
}
