import * as React from 'react';
import * as Mui from '@mui/material';
import * as MuiIcons from '@mui/icons-material';
import { getAuth, EmailAuthProvider, reauthenticateWithCredential, updatePassword, updateEmail } from 'firebase/auth';
import { ErrorMessages, ErrorType } from '../components/Login/ErrorMessage';
import { useTranslation } from 'react-i18next';

export default function SettingsPage() {
    // 0 close; 1 open verification; 2 open change email; 3 change password
    const [open, setOpen] = React.useState(0);
    // 0 change email, 1 change password
    const [selectedIndex, setSelectedIndex] = React.useState(-1);
    const [emailField, setEmailField] = React.useState('');
    const [passwordField, setPasswordField] = React.useState('');
    const [newPasswordField, setNewPasswordField] = React.useState('');
    const [newPasswordFieldConfirm, setNewPasswordFieldConfirm] = React.useState('');
    const [passwordShown, setPasswordShown] = React.useState(false);
    const [repeatPasswordShown, setRepeatPasswordShown] = React.useState(false);
    const [errorMessage, setErrorMessage] = React.useState(ErrorMessages.NO_ERROR);
    const [successChange, setSuccessChange] = React.useState(false);

    const [t] = useTranslation('common');

    const auth = getAuth();
    const user = auth.currentUser;
    const credential = EmailAuthProvider.credential(user.email, passwordField);

    const togglePasswordVisiblity = () => {
        setPasswordShown(!passwordShown);
    };
    const toggleRepeatPasswordVisiblity = () => {
        setRepeatPasswordShown(!repeatPasswordShown);
    };

    const handleClose = () => {
        setOpen(0);
        setErrorMessage(ErrorMessages.NO_ERROR);
        setSuccessChange(false);
    };

    const handleListItemClick = (event, index, openDialogue) => {
        setSelectedIndex(index);
        setOpen(openDialogue);
    };

    const validIdentity = (event, openDialogue) => {
        reauthenticateWithCredential(user, credential)
        .then(() => {
            setErrorMessage(ErrorMessages.NO_ERROR);
            setOpen(openDialogue);
            setPasswordShown(false);
            setRepeatPasswordShown(false);
        }).catch(() => {
            setErrorMessage(ErrorMessages.WRONG_PASSWORD);
        });
    };

    const changeEmail = () => {
        updateEmail(user, emailField)
        .then(() => {
            setErrorMessage(ErrorMessages.NO_ERROR);
            setSuccessChange(true);
        }).catch(error => {
            switch (error.code) {
                case 'auth/invalid-email':
                    setErrorMessage(ErrorMessages.INVALID_EMAIL);
                    break;
                case 'auth/email-already-in-use':
                    setErrorMessage(ErrorMessages.USER_EXISTS);
                    break;
                case 'auth/internal-error':
                    setErrorMessage(ErrorMessages.FIREBASE_ERROR)
                    break;
                default:
                    setErrorMessage(ErrorMessages.UNKNOWN_ERROR);
                    break;
                }
        });
    };

    const changePassword = () => {
        if (newPasswordField === newPasswordFieldConfirm) {
            updatePassword(user, newPasswordField)
            .then(() => {
                setErrorMessage(ErrorMessages.NO_ERROR);
                setSuccessChange(true);
            }).catch(error => {
                switch (error.code) {
                    case 'auth/weak-password':
                        setErrorMessage(ErrorMessages.PASSWORD_NOT_SECURE);
                        break;
                    case 'auth/internal-error':
                        setErrorMessage(ErrorMessages.FIREBASE_ERROR);
                        break;
                    default:
                        setErrorMessage(ErrorMessages.UNKNOWN_ERROR);
                        break;
                }
            });
        } else {
            if (newPasswordField.length === 0 || newPasswordFieldConfirm.length === 0) {
                setErrorMessage(ErrorMessages.PASSWORD_NULL);
            } else {
                setErrorMessage(ErrorMessages.PASSWORD_DO_NOT_MATCH);
            }
        }
    };

    const keyDown = (event) => {
        if (event.key === 'Enter') {
            if (open === 1) {
                if (selectedIndex === 0) {
                    validIdentity(event, 2);
                } else if (selectedIndex === 1) {
                    validIdentity(event, 3);
                }
            } else if (open === 2) {
                changeEmail();
            } else if (open === 3) {
                changePassword();
            }
        }
    };

    const errorMessagesCard = () => {
        if (errorMessage.type !== ErrorType.NO_ERROR) {
            return (
                <Mui.Box sx={{ py: 1 }}>
                    <Mui.Alert
                        severity='error'
                        action={
                        <Mui.IconButton
                            aria-label='close'
                            color='inherit'
                            size='small'
                            onClick={() => {
                                setErrorMessage(ErrorMessages.NO_ERROR);
                            }}
                        >
                            <MuiIcons.Close fontSize='inherit' />
                        </Mui.IconButton>
                        }
                    >
                        <strong>Error:</strong> {errorMessage.message}
                    </Mui.Alert>
                </Mui.Box>
            );
        }
    }

    const successMessageCard = () => {
        return (
            <Mui.Box sx={{ py: 1 }}>
                <Mui.Alert
                    severity='success'
                    action={
                    <Mui.IconButton
                        aria-label='close'
                        color='inherit'
                        size='small'
                        onClick={() => {
                            setErrorMessage(ErrorMessages.NO_ERROR);
                        }}
                    >
                        <MuiIcons.Close fontSize='inherit' />
                    </Mui.IconButton>
                    }
                >
                    {t('settings.successfullyUpdated')}
                </Mui.Alert>
            </Mui.Box>
        );
    }

    return (
        <Mui.Box>
            <Mui.Box
                marginX={5}
                marginY={2}
            >
                <Mui.Typography variant='h3' textAlign='center'>
                    {t('settings.settings')}
                </Mui.Typography>

                <Mui.List>
                    <Mui.ListItemButton
                        selected={selectedIndex === 0}
                        onClick={(event) => handleListItemClick(event, 0, 1)}
                    >
                        <Mui.ListItemIcon>
                            <MuiIcons.Email />
                        </Mui.ListItemIcon>
                        <Mui.ListItemText primary={t('settings.changeEmail')} />
                    </Mui.ListItemButton>
                    <Mui.ListItemButton
                        selected={selectedIndex === 1}
                        onClick={(event) => handleListItemClick(event, 1, 1)}
                    >
                        <Mui.ListItemIcon>
                            <MuiIcons.Password />
                        </Mui.ListItemIcon>
                        <Mui.ListItemText primary={t('settings.changePassword')} />
                    </Mui.ListItemButton>
                </Mui.List>
            </Mui.Box>

            <Mui.Dialog open={open === 1} onClose={handleClose}>
                <Mui.DialogTitle>{selectedIndex === 0 ? t('settings.changeEmail') : t('settings.changePassword')}</Mui.DialogTitle>
                    <Mui.DialogContent>
                        {errorMessagesCard()}
                        <Mui.DialogContentText>
                            {t('settings.confirmIdentity')}
                        </Mui.DialogContentText>
                        <Mui.TextField
                            error={errorMessage.type !== ErrorType.NO_ERROR && passwordField.length === 0}
                            margin='normal'
                            required
                            fullWidth
                            name='password'
                            label={t('settings.password')}
                            type={passwordShown ? 'text' : 'password'}
                            id='password'
                            value={passwordField}
                            onChange={(event) => setPasswordField(event.target.value)}
                            onKeyDown={(event) => keyDown(event)}
                            autoComplete='off'
                            InputProps={{
                                endAdornment: (
                                <Mui.IconButton onClick={togglePasswordVisiblity} >
                                    <Mui.Tooltip title={passwordShown ? t('settings.hide') : t('settings.shown')}>
                                    {passwordShown
                                        ? <MuiIcons.Visibility color='primary' sx={{ mx: 1 }} />
                                        : <MuiIcons.VisibilityOff color='primary' sx={{ mx: 1 }} />}
                                    </Mui.Tooltip>
                                </Mui.IconButton >
                                )
                            }}
                        />
                        </Mui.DialogContent>
                    <Mui.DialogActions>
                    <Mui.Button onClick={handleClose}>Cancel</Mui.Button>
                    <Mui.Button onClick={(event) => {
                        if (selectedIndex === 0) {
                            validIdentity(event, 2);
                        } else if (selectedIndex === 1) {
                            validIdentity(event, 3);
                        }
                    }}>
                        {t('settings.submit')}
                    </Mui.Button>
                </Mui.DialogActions>
            </Mui.Dialog>

            <Mui.Dialog open={open === 2} onClose={handleClose}>
                <Mui.DialogTitle>{t('settings.changeEmail')}</Mui.DialogTitle>
                    <Mui.DialogContent>
                        {errorMessagesCard()}
                        {successChange && successMessageCard()}
                        <Mui.DialogContentText>
                            {t('settings.enterNewEmail')}
                        </Mui.DialogContentText>
                        <Mui.TextField
                            error={errorMessage.type === ErrorType.EMAIL}
                            margin='normal'
                            required
                            fullWidth
                            id='email'
                            label={t('settings.emailAdress')}
                            name='email'
                            type='email'
                            autoComplete='email'
                            autoFocus
                            value={emailField}
                            onChange={(event) => setEmailField(event.target.value)}
                            onKeyDown={(event) => keyDown(event)}
                            />
                        </Mui.DialogContent>
                    <Mui.DialogActions>
                    <Mui.Button onClick={handleClose}>{successChange ? t('settings.close') : t('settings.cancel')}</Mui.Button>
                    {!successChange &&
                        <Mui.Button onClick={changeEmail}>{t('settings.submit')}</Mui.Button>
                    }
                </Mui.DialogActions>
            </Mui.Dialog>

            <Mui.Dialog open={open === 3} onClose={handleClose}>
                <Mui.DialogTitle>{t('settings.changePassword')}</Mui.DialogTitle>
                    <Mui.DialogContent>
                        {errorMessagesCard()}
                        {successChange && successMessageCard()}
                        <Mui.DialogContentText>
                            {t('settings.enterNewPassword')}
                        </Mui.DialogContentText>
                        <Mui.TextField
                            error={errorMessage.type === ErrorType.PASSWORD}
                            margin='normal'
                            required
                            fullWidth
                            name='password'
                            label={t('settings.password')}
                            type={passwordShown ? 'text' : 'password'}
                            id='password'
                            value={newPasswordField}
                            onChange={(event) => setNewPasswordField(event.target.value)}
                            onKeyDown={(event) => keyDown(event)}
                            autoComplete='off'
                            InputProps={{
                                endAdornment: (
                                <Mui.IconButton onClick={togglePasswordVisiblity} >
                                    <Mui.Tooltip title={passwordShown ? t('settings.hide') : t('settings.shown')}>
                                    {passwordShown
                                        ? <MuiIcons.Visibility color='primary' sx={{ mx: 1 }} />
                                        : <MuiIcons.VisibilityOff color='primary' sx={{ mx: 1 }} />}
                                    </Mui.Tooltip>
                                </Mui.IconButton >
                                )
                            }}
                        />
                        <Mui.TextField
                            error={errorMessage.type === ErrorType.PASSWORD}
                            margin='normal'
                            required
                            fullWidth
                            name='passwordConfirm'
                            label={t('settings.confirmPassword')}
                            type={repeatPasswordShown ? 'text' : 'password'}
                            id='passwordConfirm'
                            autoComplete='new-password'
                            value={newPasswordFieldConfirm}
                            onChange={(event) => setNewPasswordFieldConfirm(event.target.value)}
                            onKeyDown={(event) => keyDown(event)}
                            InputProps={{
                                endAdornment: (
                                <Mui.IconButton onClick={toggleRepeatPasswordVisiblity} >
                                    <Mui.Tooltip title={repeatPasswordShown ? t('settings.hide') : t('settings.shown')}>
                                    {repeatPasswordShown ? <MuiIcons.Visibility color='primary' sx={{ mx: 1 }} /> : <MuiIcons.VisibilityOff color='primary' sx={{ mx: 1 }} />}
                                    </Mui.Tooltip>
                                </Mui.IconButton >
                                )
                            }}
                        />
                        </Mui.DialogContent>
                    <Mui.DialogActions>
                    <Mui.Button onClick={handleClose}>{successChange ? t('settings.close') : t('settings.cancel')}</Mui.Button>
                    {!successChange &&
                        <Mui.Button onClick={changePassword}>{t('settings.submit')}</Mui.Button>
                    }
                </Mui.DialogActions>
            </Mui.Dialog>

        </Mui.Box>
    );
}
