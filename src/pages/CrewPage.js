import * as React from 'react';
import * as Mui from '@mui/material';
import { useParams } from 'react-router-dom';
import { getAuth } from 'firebase/auth';
import Communication from '../service/communication';
import Crew from '../httpObjects/Crew';
import MovieCard from '../components/Cards/MovieCard';
import HowToWatchCarousel from '../components/HowToWatchCarousel';
import LoadingCarousel from '../components/LoadingCarousel';
import { useTranslation } from 'react-i18next';

export default function CrewPage() {
    const { personID } = useParams();

    const [crew, setCrew] = React.useState(null);
    const [loading, setLoading] = React.useState(false);

    const [t] = useTranslation('common');

    React.useEffect(() => {
        if (personID !== undefined && personID !== null && personID !== '') {
            setLoading(true);
            getAuth().currentUser.getIdToken().then(token => {
                Communication.sendGetRequest('api/private/crew/' + personID, token).then(async (response) => {
                    const result = await response.json();
                    if (result === null || response.status !== 200) {
                        setCrew(null);
                        setLoading(false);
                    } else {
                        setCrew(new Crew(result));
                        setLoading(false);
                    }
                });
            }).catch((_) => {
                setCrew(undefined);
                setLoading(false);
            });
        }
    }, [personID]);

    const displayMovies = () => {
        const movies = [];
        for (const movieID in crew.movies) {
            const movie = crew.movies[movieID];
            movies.push(<MovieCard key={movie.movie_id} movie={movie}></MovieCard>);
        }
        return movies;
    };

    const displayCrew = () => {
        if (crew === undefined) {
            return <><Mui.Stack direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={3}
            ><Mui.Typography variant='h5'>Server error.</Mui.Typography></Mui.Stack></>
        }
        if (crew === null) {
            return <><Mui.Stack direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={3}
            ><Mui.Typography variant='h5'>Error.</Mui.Typography></Mui.Stack></>
        }

        let nbOfSlidesToScroll = 3;
        while (Object.keys(crew.movies).length % nbOfSlidesToScroll !== 0) {
            nbOfSlidesToScroll--;
        }
        const settingsMovies = {
            infinite: true,
            slidesToShow: Math.min(Object.keys(crew.movies).length, 6),
            slidesToScroll: nbOfSlidesToScroll
        };

        return (
            <>
                <Mui.Box display='flex' justifyContent='flex-start' mx={10} p={1}>
                    <Mui.Box p={1}>
                        {crew.image_url && <img src={crew.image_url} />}
                        {!crew.image_url && <img src='/images/waiting.png' />}
                    </Mui.Box>
                    <Mui.Box p={1}>
                        <Mui.Typography style={{ marginBottom: '0' }} variant='h3' gutterBottom>
                            {crew.name} ({crew.birth_year} -- {crew.death_year && <>{crew.death_year}</>})
                        </Mui.Typography>
                        {
                            crew.birth_place && <Mui.Typography variant='h6' gutterBottom>
                                {t('crew.bornIn')} {crew.birth_place}
                            </Mui.Typography>
                        }
                        {
                            crew.biography && <Mui.Typography style={{ marginTop: '20px' }} variant='h4' gutterBottom>
                                {t('crew.biography')}
                            </Mui.Typography>}
                        {
                            crew.biography && <Mui.Typography variant='h6' gutterBottom>
                                {crew.biography}
                            </Mui.Typography>
                        }
                    </Mui.Box>
                </Mui.Box>
                <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
                    <Mui.Box p={1}>
                        <Mui.Typography variant="h4" gutterBottom>{t('crew.movies')}:</Mui.Typography>
                    </Mui.Box>
                </Mui.Box>
                <Mui.Box display="block" mx={10} p={1}>
                    <HowToWatchCarousel {...settingsMovies}>
                        {displayMovies()}
                    </HowToWatchCarousel>
                </Mui.Box>
            </>
        );
    };

    const displayLoading = () => {
        return (
            <>
                <Mui.Box display='flex' justifyContent='flex-start' mx={10} p={1}>
                    <Mui.Box p={1}>
                        <img src={'/images/waiting.png'} />
                    </Mui.Box>
                    <Mui.Box p={1}>
                        <Mui.Typography style={{ marginBottom: '0' }} variant='h3' gutterBottom>
                            <Mui.CircularProgress />
                        </Mui.Typography>
                        <Mui.Typography variant='h6' gutterBottom>
                            <Mui.CircularProgress />
                        </Mui.Typography>
                    </Mui.Box>
                </Mui.Box>
                <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
                    <Mui.Box p={1}>
                        <Mui.Typography variant="h4" gutterBottom>{t('crew.movies')}:</Mui.Typography>
                    </Mui.Box>
                </Mui.Box>
                <LoadingCarousel cardKey={'movies'} slidesToShow={6}></LoadingCarousel>
            </>
        );
    };

    const noCrew = () => {
        return (
            <Mui.Stack direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={3}
            >
                <Mui.Typography variant='h5'>{t('crew.noCorrespondingCrew')}</Mui.Typography>
            </Mui.Stack>
        );
    };

    return (
        <>
            {loading && displayLoading()}
            {!loading && !crew && noCrew()}
            {!loading && crew && displayCrew()}
        </>
    );
}
