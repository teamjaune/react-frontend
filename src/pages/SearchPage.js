import * as React from 'react';
import * as Mui from '@mui/material';
import { useParams } from 'react-router-dom';
import { getAuth } from 'firebase/auth';
import Communication from '../service/communication';
import User from '../httpObjects/User';
import SearchMoviesTab from '../components/Search/SearchMoviesTab';
import SearchCrewTab from '../components/Search/SearchCrewTab';
import SearchCompaniesTab from '../components/Search/SearchCompaniesTab';
import SearchUsersTab from '../components/Search/SearchUsersTab';
import SearchLoadingTab from '../components/Search/SearchLoadingTab';
import SearchMovie from './../httpObjects/Search/SearchMovie';
import SearchCompany from './../httpObjects/Search/SearchCompany';
import SearchCrew from './../httpObjects/Search/SearchCrew';
import { useTranslation } from 'react-i18next';

export default function SearchPage() {
    const { searchText } = useParams();
    const [value, setValue] = React.useState(0);

    const [loadingMovies, setLoadingMovies] = React.useState(true);
    const [loadingCrewMembers, setLoadingCrewMembers] = React.useState(true);
    const [loadingCompanies, setLoadingCompanies] = React.useState(true);
    const [loadingUsers, setLoadingUsers] = React.useState(true);

    const [movies, setMovies] = React.useState([]);
    const [crewMembers, setCrewMembers] = React.useState([]);
    const [companies, setCompanies] = React.useState([]);
    const [users, setUsers] = React.useState([]);

    const [moviesOffset, setMoviesOffset] = React.useState(0);
    const [crewMembersOffset, setCrewMembersOffset] = React.useState(0);
    const [companiesOffset, setCompaniesOffset] = React.useState(0);
    const [usersOffset, setUsersOffset] = React.useState(0);

    const [noMoreMovies, setNoMoreMovies] = React.useState(false);
    const [noMoreCrewMembers, setNoMoreCrewMembers] = React.useState(false);
    const [noMoreCompanies, setNoMoreCompanies] = React.useState(false);
    const [noMoreUsers, setNoMoreUsers] = React.useState(false);

    const [searchTextSave, setSearchTextSave] = React.useState('');
    const [previousScrollLocation, setPreviousScrollLocation] = React.useState('');

    const [t] = useTranslation('common');

    const handleChange = (_, newValue) => {
        setValue(newValue);
    };

    React.useEffect(() => {
        if (searchText !== searchTextSave) {
            setPreviousScrollLocation(0);
        }
        window.scrollTo({ top: previousScrollLocation, behavior: 'instant' });
        if (searchText !== searchTextSave) {
            setSearchTextSave(searchText);

            setMovies([]);
            setLoadingMovies(true);
            setNoMoreMovies(false);
            fetchMovies(true);

            setCrewMembers([]);
            setLoadingCrewMembers(true);
            setNoMoreCrewMembers(false);
            fetchCrewMembers(true);

            setCompanies([]);
            setLoadingCompanies(true);
            setNoMoreCompanies(false);
            fetchCompanies(true);

            setUsers([]);
            setLoadingUsers(true);
            setNoMoreUsers(false);
            fetchUsers(true);
        }
    }, [searchText, loadingMovies, loadingCrewMembers, loadingCompanies, loadingUsers, moviesOffset, crewMembersOffset, companiesOffset, usersOffset]);

    function TabPanel(props) {
        const { children, value, index, ...other } = props;

        return (
            <div
                role="tabpanel"
                hidden={value !== index}
                id={`simple-tabpanel-${index}`}
                aria-labelledby={`simple-tab-${index}`}
                {...other}
            >
                {value === index && (
                    <Mui.Box sx={{ p: 3 }}>
                        {children}
                    </Mui.Box>
                )}
            </div>
        );
    }

    const getPreviousResults = (reset, previous) => {
        if (reset) {
            return [];
        }
        return previous !== undefined && previous !== null ? previous : [];
    }

    const fetchMovies = (reset) => {
        setLoadingMovies(true);
        getAuth().currentUser.getIdToken().then(token => {
            const offset = reset ? 0 : moviesOffset;
            Communication.sendPostRequest('api/private/search/movie', token, JSON.stringify({ searchText: searchText, offset: offset })).then(async (response) => {
                const result = await response.json();
                if (result === null || !Array.isArray(result)) {
                    setMovies([]);
                    setLoadingMovies(false);
                } else {
                    const res = getPreviousResults(reset, movies);
                    const oldLength = res.length;
                    for (const movie of result) {
                        res.push(new SearchMovie(movie));
                    }
                    if (res.length === oldLength) {
                        setNoMoreMovies(true);
                    }
                    setMovies(res);
                    const newOffset = offset + 10;
                    setMoviesOffset(newOffset);
                    setLoadingMovies(false);
                }
            }).catch((_) => {
                setMovies(undefined);
                setLoadingMovies(false);
            });
        });
    };

    const fetchCrewMembers = (reset) => {
        setLoadingCrewMembers(true);
        getAuth().currentUser.getIdToken().then(token => {
            const offset = reset ? 0 : crewMembersOffset;
            Communication.sendPostRequest('api/private/search/crew', token, JSON.stringify({ searchText: searchText, offset: offset })).then(async (response) => {
                const result = await response.json();
                if (result === null || !Array.isArray(result)) {
                    setCrewMembers([]);
                    setLoadingCrewMembers(false);
                } else {
                    const res = getPreviousResults(reset, crewMembers);
                    const oldLength = res.length;
                    for (const crew of result) {
                        res.push(new SearchCrew(crew));
                    }
                    if (res.length === oldLength) {
                        setNoMoreCrewMembers(true);
                    }
                    setCrewMembers(res);
                    const newOffset = offset + 10;
                    setCrewMembersOffset(newOffset);
                    setLoadingCrewMembers(false);
                }
            }).catch((_) => {
                setCrewMembers(undefined);
                setLoadingCrewMembers(false);
            });
        });
    };

    const fetchCompanies = (reset) => {
        setLoadingCompanies(true);
        getAuth().currentUser.getIdToken().then(token => {
            const offset = reset ? 0 : companiesOffset;
            Communication.sendPostRequest('api/private/search/companies', token, JSON.stringify({ searchText: searchText, offset: offset })).then(async (response) => {
                const result = await response.json();
                if (result === null || !Array.isArray(result)) {
                    setCompanies([]);
                    setLoadingCompanies(false);
                } else {
                    const res = getPreviousResults(reset, companies);
                    const oldLength = res.length;
                    for (const company of result) {
                        res.push(new SearchCompany(company));
                    }
                    if (res.length === oldLength) {
                        setNoMoreCompanies(true);
                    }
                    setCompanies(res);
                    const newOffset = offset + 10;
                    setCompaniesOffset(newOffset);
                    setLoadingCompanies(false);
                }
            }).catch((_) => {
                setCompanies(undefined);
                setLoadingCompanies(false);
            });
        });
    };

    const fetchUsers = (reset) => {
        setLoadingUsers(true);
        getAuth().currentUser.getIdToken().then(token => {
            const offset = reset ? 0 : usersOffset;
            Communication.sendPostRequest('api/private/search/user', token, JSON.stringify({ searchText: searchText, offset: offset })).then(async (response) => {
                const result = await response.json();
                if (result === null || !Array.isArray(result)) {
                    setUsers([]);
                    setLoadingUsers(false);
                } else {
                    const res = getPreviousResults(reset, users);
                    const oldLength = res.length;
                    for (const user of result) {
                        res.push(new User(user));
                    }
                    if (res.length === oldLength) {
                        setNoMoreUsers(true);
                    }
                    setUsers(res);
                    const newOffset = offset + 10;
                    setUsersOffset(newOffset);
                    setLoadingUsers(false);
                }
            }).catch((_) => {
                setUsers(undefined);
                setLoadingUsers(false);
            });
        });
    };

    const fetchMore = () => {
        setPreviousScrollLocation(window.scrollY);
        switch (value) {
            case 0:
                fetchMovies(false);
                break;
            case 1:
                fetchCrewMembers(false);
                break;
            case 2:
                fetchCompanies(false);
                break;
            case 3:
                fetchUsers(false);
                break;
        }
    };

    return (
        <>
            <Mui.Box sx={{ width: '100%' }}>
                <Mui.Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                    <Mui.Tabs value={value} onChange={handleChange} centered>
                        <Mui.Tab label={t('search.movies')} />
                        <Mui.Tab label={t('search.crew')} />
                        <Mui.Tab label={t('search.companies')} />
                        <Mui.Tab label={t('search.users')} />
                    </Mui.Tabs>
                </Mui.Box>
                <TabPanel value={value} index={0}>
                    <SearchMoviesTab movies={movies} loading={loadingMovies} setLoading={setLoadingMovies}></SearchMoviesTab>
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <SearchCrewTab crewMembers={crewMembers} loading={loadingCrewMembers} setLoading={setLoadingCrewMembers}></SearchCrewTab>
                </TabPanel>
                <TabPanel value={value} index={2}>
                    <SearchCompaniesTab companies={companies} loading={loadingCompanies} setLoading={setLoadingCompanies}></SearchCompaniesTab>
                </TabPanel>
                <TabPanel value={value} index={3}>
                    <SearchUsersTab users={users} loading={loadingUsers} setLoading={setLoadingUsers}></SearchUsersTab>
                </TabPanel>
                <Mui.Grid
                    container
                    direction="row"
                    justifyContent="space-evenly"
                    alignItems="center"
                >
                    {value === 0 && loadingMovies && <SearchLoadingTab tab={'movies'} displayRating={true}></SearchLoadingTab>}
                    {value === 1 && loadingCrewMembers && <SearchLoadingTab tab={'crew'} displayRating={false}></SearchLoadingTab>}
                    {value === 2 && loadingCompanies && <SearchLoadingTab tab={'companies'} displayRating={false}></SearchLoadingTab>}
                    {value === 3 && loadingUsers && <SearchLoadingTab tab={'users'} displayRating={false}></SearchLoadingTab>}

                    {value === 0 && !loadingMovies && movies && movies.length > 0 && !noMoreMovies && <Mui.Button onClick={fetchMore} variant="contained" style={{ width: '80%' }}>{t('global.seeMore')}</Mui.Button>}
                    {value === 1 && !loadingCrewMembers && crewMembers && crewMembers.length > 0 && !noMoreCrewMembers && <Mui.Button onClick={fetchMore} variant="contained" style={{ width: '80%' }}>{t('global.seeMore')}</Mui.Button>}
                    {value === 2 && !loadingCompanies && companies && companies.length > 0 && !noMoreCompanies && <Mui.Button onClick={fetchMore} variant="contained" style={{ width: '80%' }}>{t('global.seeMore')}</Mui.Button>}
                    {value === 3 && !loadingUsers && users && users.length > 0 && !noMoreUsers && <Mui.Button onClick={fetchMore} variant="contained" style={{ width: '80%' }}>{t('global.seeMore')}</Mui.Button>}
                </Mui.Grid>
            </Mui.Box>
        </>
    );
}
