import * as React from 'react';
import * as Mui from '@mui/material';
import { getAuth } from 'firebase/auth';
import Communication from '../service/communication';
import ScrollCarousel from '../components/ScrollCarousel';
import MovieCard from '../components/Cards/MovieCard';
import WaitingCard from '../components/Cards/WaitingCard';
import SearchMovie from '../httpObjects/Search/SearchMovie';
import Movie from '../httpObjects/Movie';
import { styled, useTheme } from '@mui/material/styles';

export default function SuggestionsPage() {
  const theme = useTheme();

  const MovieLink = styled(Mui.Link)(({ _ }) => ({
    'fontWeight': 'bold',
    'color': theme.palette.text.primary,
    'textDecoration': 'none',
    '&:hover': {
        'fontWeight': 'bold',
        'color': theme.palette.text.primary,
        'cursor': 'pointer',
        'textDecoration': 'underline'
    }
  }));

  const NUMBER_OF_SUGGESTIONS = 5;
  const [globalSuggestionsloading, setGlobalSuggestionsLoading] = React.useState(true);
  const [globalSuggestions, setGlobalSuggestions] = React.useState([]);
  const [lastRatedSuggestions, setLastRatedSuggestions] = React.useState([]);
  const [lastRatedMovies, setLastRatedMovies] = React.useState([]);
  const [pendingReceive, setPendingReceive] = React.useState(false);

  React.useEffect(() => {
    getAuth().currentUser.getIdToken().then(token => {
      getSuggestionsForAllMovies(token);
      for (let i = 0; i < NUMBER_OF_SUGGESTIONS; i++) {
        getSuggestionsForTheLastMovieAtIndex(token, i);
      }
    });
  }, []);

  const getSuggestionsForAllMovies = (token) => {
    Communication.sendGetRequest('api/private/suggestions/getGlobalSuggestions', token).then(async (response) => {
      const result = await response.json();
      if (result === null || !Array.isArray(result)) {
        setGlobalSuggestions([]);
        setGlobalSuggestionsLoading(false);
      } else {
        const res = [];
        for (const movie of result) {
          res.push(new SearchMovie(movie));
        }
        setGlobalSuggestions(res);
        setGlobalSuggestionsLoading(false);
      }
    }).catch((_) => {
      setGlobalSuggestions([]);
      setGlobalSuggestionsLoading(false);
    });
  }

  const getSuggestionsForTheLastMovieAtIndex = (token, index) => {
    Communication.sendGetRequest('api/private/suggestions/getLastRatedSuggestions/' + index, token).then(async (response) => {
      const result = await response.json();
      while (pendingReceive);//eslint-disable-line
      setPendingReceive(true);
      if (result !== null || result.last_rated_movie !== null || result.suggestions !== null || Array.isArray(result.suggestions)) {
        const resSuggestions = lastRatedSuggestions;
        const resLastRatedMovies = lastRatedMovies;
        resLastRatedMovies.push(new Movie(result.last_rated_movie))
        const currentSuggestions = [];
        for (const movie of result.suggestions) {
          currentSuggestions.push(new SearchMovie(movie));
        }
        resSuggestions.push(currentSuggestions);
        setLastRatedSuggestions(resSuggestions);
        setLastRatedMovies(resLastRatedMovies);
        setPendingReceive(false);
      } else {
        const resSuggestions = lastRatedSuggestions;
        const resLastRatedMovies = lastRatedMovies;
        resLastRatedMovies.push(null)
        resSuggestions.push(null);
        setLastRatedSuggestions(resSuggestions);
        setLastRatedMovies(resLastRatedMovies);
        setPendingReceive(false);
      }
    }).catch((_) => {
        const resSuggestions = lastRatedSuggestions;
        const resLastRatedMovies = lastRatedMovies;
        resLastRatedMovies.push(null)
        resSuggestions.push(null);
        setLastRatedSuggestions(resSuggestions);
        setLastRatedMovies(resLastRatedMovies);
        setPendingReceive(false);
    });
  }

  const getGlobalSuggestionsMovies = () => {
    const movies = [];
    for (const movie of globalSuggestions) {
      movies.push(<MovieCard key={movie.movie_id} movie={movie}></MovieCard>);
    }
    return movies;
  }

  const displayGlobalSuggestionsMovies = () => {
    const res = [];
    res.push(
        <>
          <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
            <Mui.Box p={1}>
                <Mui.Typography variant="h4" gutterBottom>Our suggestions for you:</Mui.Typography>
            </Mui.Box>
          </Mui.Box>
        </>
    );

    if (globalSuggestionsloading) {
      res.push(
        <>
          <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
            {displayLoadingCards(0)}
          </Mui.Box>
        </>
      );
    } else {
      res.push(
        <>
          <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
            <ScrollCarousel swiping={true} show={5} slide={3} transition={0.5}>
              {getGlobalSuggestionsMovies()}
            </ScrollCarousel>
          </Mui.Box>
        </>
      );
    }
    return res;
  };

  const displayLastRatedSuggestionsMovies = (index) => {
    const movies = [];
    for (const movie of lastRatedSuggestions[index]) {
      const key = movie.movie_id + '_' + index;
      movies.push(<MovieCard key={key} movie={movie}></MovieCard>);
    }
    return movies;
  };

  const displayLoadingCards = (index) => {
    const movies = [];
    for (let i = 0; i < (2 * NUMBER_OF_SUGGESTIONS); i++) {
      const key = index + '_' + i;
      movies.push(<WaitingCard key={key}></WaitingCard>);
    }
    return movies;
  };

  const displayAllLastRatedSuggestions = () => {
    const res = [];
    for (let i = 0; i < NUMBER_OF_SUGGESTIONS; i++) {
      if (lastRatedMovies.length <= i) {
        res.push(<>
                    <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
                      <Mui.Box p={1}>
                          <Mui.Typography variant="h4" gutterBottom>Because you recently liked <Mui.CircularProgress />:</Mui.Typography>
                      </Mui.Box>
                    </Mui.Box>
                    <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
                      {displayLoadingCards(i + 1)}
                    </Mui.Box>
                  </>
                );
      } else {
        if (lastRatedMovies[i] != null) {
          res.push(<>
                      <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
                        <Mui.Box p={1}>
                            <Mui.Typography variant="h4" gutterBottom>Because you recently liked <MovieLink href={'/Movie/' + lastRatedMovies[i].movie_id}>{lastRatedMovies[i].getTitle()}</MovieLink>:</Mui.Typography>
                        </Mui.Box>
                      </Mui.Box>
                      <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
                        <ScrollCarousel swiping={true} show={5} slide={3} transition={0.5}>
                          {displayLastRatedSuggestionsMovies(i)}
                        </ScrollCarousel>
                      </Mui.Box>
                    </>
                  );
        }
      }
    }
    return res;
  }

  return (
    <>
      {displayGlobalSuggestionsMovies()}
      <>
        {displayAllLastRatedSuggestions()}
      </>
    </>
  );
}
