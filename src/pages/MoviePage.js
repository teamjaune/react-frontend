import * as React from 'react';
import * as Mui from '@mui/material';
import * as MuiIcons from '@mui/icons-material';
import { styled, useTheme } from '@mui/material/styles';
import { useParams } from 'react-router-dom';
import { getAuth } from 'firebase/auth';
import Communication from '../service/communication';
import Movie from '../httpObjects/Movie';
import UserRating from '../httpObjects/Movie/UserRating';
import CrewCard from '../components/Cards/CrewCard';
import UserRatingCard from '../components/Cards/UserRatingCard';
import RatingDisplay from '../components/RatingDisplay';
import HowToWatchCarousel from '../components/HowToWatchCarousel';
import LoadingCarousel from '../components/LoadingCarousel';
import WatchMethodCard from '../components/Cards/WatchMethodCard';
import CountryList from '../components/CountryList';
import { useTranslation } from 'react-i18next';

export default function MoviePage() {
    const { movieID } = useParams();
    const theme = useTheme();

    const [movie, setMovie] = React.useState(null);
    const [loading, setLoading] = React.useState(false);
    const [showingWatchMethods, setShowingWatchMethods] = React.useState(false);
    const [currentCountry, setCurrentCountry] = React.useState('FR');

    const [userRatings, setUserRatings] = React.useState(null);
    const [userRatingsLoading, setUserRatingsLoading] = React.useState(false);

    const [t] = useTranslation('common');

    React.useEffect(() => {
        if (movieID !== undefined && movieID !== null && movieID !== '') {
            setLoading(true);
            setUserRatingsLoading(true);
            getAuth().currentUser.getIdToken().then(token => {
                Communication.sendGetRequest('api/private/movie/' + movieID, token).then(async (response) => {
                    const result = await response.json();
                    if (result === null || response.status !== 200) {
                        setMovie(null);
                        setLoading(false);
                    } else {
                        setMovie(new Movie(result));
                        setLoading(false);
                    }
                }).catch((_) => {
                    setMovie(undefined);
                    setLoading(false);
                });
                Communication.sendGetRequest('api/private/movie/' + movieID + '/ratings', token).then(async (response) => {
                    const result = await response.json();
                    if (result === null) {
                        setUserRatings(null);
                        setUserRatingsLoading(false);
                    } else {
                        const res = [];
                        for (const ur of result) {
                            res.push(new UserRating(ur));
                        }
                        setUserRatings(res);
                        setUserRatingsLoading(false);
                    }
                }).catch((_) => {
                    setUserRatings(undefined);
                    setUserRatingsLoading(false);
                });
            });
        }
    }, [movieID]);

    const displayGenres = () => {
        if (movie.genres === null || movie.genres === undefined || movie.genres.length === 0) {
            return (<></>);
        }
        return (
            <>
                <Mui.Typography style={{ marginTop: '20px' }} variant='h5'>Genres:</Mui.Typography>
                <Mui.Grid
                    container
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="center"
                    style={{ marginTop: '10px' }}
                >
                    {movie.genres.map(genre => (<Mui.Chip style={{ margin: '3px' }} size='medium' key={genre} label={genre} />))}
                </Mui.Grid>
            </>
        );
    };

    const displayKeywords = () => {
        if (movie.keywords === null || movie.keywords === undefined || movie.keywords.length === 0) {
            return (<></>);
        }
        return (
            <>
                <Mui.Typography style={{ marginTop: '20px' }} variant='h5'>{t('movie.keywords')}</Mui.Typography>
                <Mui.Grid
                    container
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="center"
                    style={{ marginTop: '10px' }}
                >
                    {movie.keywords.map(keyword => (<Mui.Chip style={{ margin: '3px' }} size='medium' key={keyword} label={keyword} />))}
                </Mui.Grid>
            </>
        );
    };

    const Chip = styled(Mui.Chip)(({ _ }) => ({
        'color': '#000000',
        '&:hover': {
            'color': '#000000',
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    const displayProductionCompanies = () => {
        if (movie.production_companies === null || movie.production_companies === undefined || movie.production_companies.length === 0) {
            return (<></>);
        }
        return (
            <>
                <Mui.Typography style={{ marginTop: '20px' }} variant='h5'>{t('movie.productionCompanies')}</Mui.Typography>
                <Mui.Grid
                    container
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="center"
                    style={{ marginTop: '10px' }}
                >
                    {movie.production_companies.map(company => (<Chip component='a' href={'/Company/' + company.company_id} style={{ color: theme.palette.text.primary, margin: '3px' }} size='medium' key={company.company_id} label={company.name} />))}
                </Mui.Grid>
            </>
        );
    };

    const displayMovieInformations = () => {
        return (
            <>
                <Mui.Typography style={{ marginBottom: '0' }} variant='h4' gutterBottom>
                    {movie.getTitle()} ({movie.start_year}{movie.title_type === 'tvSeries' && <> -- {movie.end_year}</>})
                </Mui.Typography>
                {movie.getTitle() !== movie.original_title && <Mui.Typography variant='h6' gutterBottom>
                    {t('movie.originalTitle')} {movie.original_title}
                </Mui.Typography>}
                {displayProductionCompanies()}
                <Mui.Typography variant='h5' gutterBottom style={{ marginTop: '20px' }}>
                    {movie.is_adult && <MuiIcons.Explicit style={{ marginTop: '-1' }}></MuiIcons.Explicit>}
                    {movie.getTitleType()}
                    - {movie.getDuration()}
                </Mui.Typography>
                <Mui.Stack direction="row"
                    justifyContent="flex-start"
                    alignItems="center"
                    spacing={3}
                    style={{ marginTop: '20px' }}
                >
                    <RatingDisplay id="rating" title={t('movie.myRating')} movie={movie} rating={movie.user_rating / 2}></RatingDisplay>
                    <Mui.Divider orientation="vertical" flexItem />
                    <RatingDisplay readOnly={true} color='#1976d2' title={t('movie.IMDBRating')} rating={movie.average_rating / 2}></RatingDisplay>
                    <Mui.Divider orientation="vertical" flexItem />
                    <RatingDisplay readOnly={true} color='#1976d2' title={t('movie.averageRating')} rating={movie.our_average_rating / 2}></RatingDisplay>
                </Mui.Stack>
                <Mui.Typography style={{ marginTop: '20px' }} variant='h4' gutterBottom>{t('movie.overview')}</Mui.Typography>
                <Mui.Typography variant='h6' gutterBottom>{movie.synopsis}</Mui.Typography>
                {displayGenres()}
                {displayKeywords()}
            </>
        );
    };

    const countrySelected = (selectedCountry) => {
        setCurrentCountry(selectedCountry);
    }

    const displayWatchMethods = () => {
        const settingsFlatrate = {
            infinite: true,
            slidesToShow: Math.min(movie.getWatchMethodsWithType('flatrate').filter(x => x.country === currentCountry).length, 10),
            slidesToScroll: Math.min(movie.getWatchMethodsWithType('flatrate').filter(x => x.country === currentCountry).length, 10)
        };

        const settingsRent = {
            infinite: true,
            slidesToShow: Math.min(movie.getWatchMethodsWithType('rent').filter(x => x.country === currentCountry).length, 10),
            slidesToScroll: Math.min(movie.getWatchMethodsWithType('rent').filter(x => x.country === currentCountry).length, 10)
        };

        const settingsBuy = {
            infinite: true,
            slidesToShow: Math.min(movie.getWatchMethodsWithType('buy').filter(x => x.country === currentCountry).length, 10),
            slidesToScroll: Math.min(movie.getWatchMethodsWithType('buy').filter(x => x.country === currentCountry).length, 10)
        };

        return (
            <>
                <a href='https://www.justwatch.com/'><img style={{ height: '30px' }} src='../images/justwatch.svg' /></a>
                <div style={{ width: '100%', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                    <Mui.Typography variant='subtitle1'><a href='https://www.justwatch.com/'>JustWatch</a> {t('movie.justWatch')}</Mui.Typography>
                    <CountryList key={movie.movie_id} countrySelected={countrySelected} currentCountry={currentCountry} countryList={movie.watch_methods_languages}></CountryList>
                </div>

                {movie.getWatchMethodsWithType('flatrate').filter(x => x.country === currentCountry).length > 0 &&
                    <>
                        <Mui.Typography variant='h5'>Streaming</Mui.Typography>
                        <HowToWatchCarousel {...settingsFlatrate}>
                            {movie.getWatchMethodsWithType('flatrate').filter(x => x.country === currentCountry).map(watchMethod => (<WatchMethodCard watchMethod={watchMethod} key={watchMethod.name}></WatchMethodCard>))}
                        </HowToWatchCarousel>
                    </>
                }
                {movie.getWatchMethodsWithType('rent').filter(x => x.country === currentCountry).length > 0 &&
                    <>
                        <Mui.Typography variant='h5'>{t('movie.rent')}</Mui.Typography>
                        <HowToWatchCarousel {...settingsRent}>
                            {movie.getWatchMethodsWithType('rent').filter(x => x.country === currentCountry).map(watchMethod => (<WatchMethodCard watchMethod={watchMethod} key={watchMethod.name}></WatchMethodCard>))}
                        </HowToWatchCarousel>
                    </>
                }
                {movie.getWatchMethodsWithType('buy').filter(x => x.country === currentCountry).length > 0 &&
                    <>
                        <Mui.Typography variant='h5'>{t('movie.buy')}</Mui.Typography>
                        <HowToWatchCarousel {...settingsBuy}>
                            {movie.getWatchMethodsWithType('buy').filter(x => x.country === currentCountry).map(watchMethod => (<WatchMethodCard watchMethod={watchMethod} key={watchMethod.name}></WatchMethodCard>))}
                        </HowToWatchCarousel>
                    </>
                }
            </>
        );
    }

    const displayMovie = () => {
        if (movie === undefined) {
            return <><Mui.Stack direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={3}
            ><Mui.Typography variant='h5'>Server error.</Mui.Typography></Mui.Stack></>
        }
        if (movie === null) {
            return <><Mui.Stack direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={3}
            ><Mui.Typography variant='h5'>Error.</Mui.Typography></Mui.Stack></>
        }

        let nbOfSlidesToScroll = 3;
        while (movie.crew.length % nbOfSlidesToScroll !== 0) {
            nbOfSlidesToScroll--;
        }
        const settingsCast = {
            infinite: true,
            slidesToShow: Math.min(movie.crew.length, 6),
            slidesToScroll: nbOfSlidesToScroll
        };

        let settingsFollowsRatings;

        if (userRatings) {
            let nbOfSlidesToScroll = 6;
            while (userRatings.length % nbOfSlidesToScroll !== 0) {
                nbOfSlidesToScroll--;
            }
            settingsFollowsRatings = {
                infinite: true,
                slidesToShow: Math.min(userRatings.length, 6),
                slidesToScroll: nbOfSlidesToScroll
            };
        }

        return (
            <>
                <Mui.Box display='flex' justifyContent='flex-start' mx={10} p={1}>
                    <Mui.Box p={1} style={{ width: '30%' }}>
                        {movie.image_url && <img style={{ width: '100%' }} src={movie.image_url} />}
                        {!movie.image_url && <img style={{ width: '100%' }} src='/images/waiting.png' />}
                        {movie.getMainWatchMethod() && <Mui.Button style={{ width: '100%', marginTop: '10px' }} variant="contained" onClick={() => setShowingWatchMethods(!showingWatchMethods)}>
                            <Mui.Typography variant='subtitle2'>{showingWatchMethods ? t('movie.generalInformations') : t('movie.whereToWatch')}</Mui.Typography>
                        </Mui.Button>}
                    </Mui.Box>
                    <Mui.Box p={1} style={{ width: '70%' }}>
                        {showingWatchMethods && displayWatchMethods()}
                        {!showingWatchMethods && displayMovieInformations()}
                    </Mui.Box>
                </Mui.Box>
                <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
                    <Mui.Box p={1}>
                        <Mui.Typography variant="h4" gutterBottom>{t('movie.cast')}:</Mui.Typography>
                    </Mui.Box>
                </Mui.Box>
                <Mui.Box display="block" mx={10} p={1}>
                    {movie.crew.length > 0 &&
                        <HowToWatchCarousel {...settingsCast}>
                            {movie.crew.map(crew => (<CrewCard key={crew.person_id} crew={crew}></CrewCard>))}
                        </HowToWatchCarousel>
                    }
                    {movie.crew.length === 0 &&
                        <Mui.Typography variant='h5'>{t('movie.noCastFound')}</Mui.Typography>
                    }
                </Mui.Box>
                <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
                    <Mui.Box p={1}>
                        <Mui.Typography variant="h4" gutterBottom>{t('movie.followingsRatings')} :</Mui.Typography>
                    </Mui.Box>
                </Mui.Box>
                <Mui.Box display="block" mx={10} p={1}>
                    {userRatingsLoading && <Mui.CircularProgress />}
                    {!userRatingsLoading && userRatings && userRatings.length > 0 &&
                        <HowToWatchCarousel {...settingsFollowsRatings}>
                            {userRatings.map(userRating => (<UserRatingCard key={userRating.id} userRating={userRating}></UserRatingCard>))}
                        </HowToWatchCarousel>
                    }
                    {!userRatingsLoading && (!userRatings || userRatings.length === 0) &&
                        <Mui.Typography variant='h5'>{t('movie.noFollowingsRatings')}</Mui.Typography>
                    }
                </Mui.Box>
            </>
        );
    };

    const displayLoadingMovieInformations = () => {
        return (
            <>
                <Mui.Typography style={{ marginBottom: '0' }} variant='h4' gutterBottom>
                    <Mui.CircularProgress />
                </Mui.Typography>
                <Mui.Typography variant='h6' gutterBottom>
                    {t('movie.originalTitle')}: <Mui.CircularProgress />
                </Mui.Typography>
                <Mui.Typography variant='h5' gutterBottom style={{ marginTop: '20px' }}>
                    <Mui.CircularProgress />
                </Mui.Typography>
                <Mui.Stack direction="row"
                    justifyContent="flex-start"
                    alignItems="center"
                    spacing={3}
                    style={{ marginTop: '20px' }}
                >
                    <RatingDisplay id="rating" title={t('movie.myRating')} movie={null} readOnly={true} rating={5}></RatingDisplay>
                    <Mui.Divider orientation="vertical" flexItem />
                    <RatingDisplay readOnly={true} color='#1976d2' title={t('movie.IMDBRating')} rating={5}></RatingDisplay>
                    <Mui.Divider orientation="vertical" flexItem />
                    <RatingDisplay readOnly={true} color='#1976d2' title={t('movie.averageRating')} rating={5}></RatingDisplay>
                </Mui.Stack>
                <Mui.Typography style={{ marginTop: '20px' }} variant='h4' gutterBottom>{t('movie.overview')}</Mui.Typography>
                <Mui.Typography variant='h6' gutterBottom><Mui.CircularProgress /></Mui.Typography>
            </>
        );
    };

    const displayLoading = () => {
        return (
            <>
                <Mui.Box display='flex' justifyContent='flex-start' mx={10} p={1}>
                    <Mui.Box p={1} style={{ width: '30%' }}>
                        <img style={{ width: '100%' }} src={'/images/waiting.png'} />
                        <Mui.Button style={{ width: '100%', marginTop: '10px' }} variant="contained">
                            <Mui.Typography variant='subtitle2'>{t('movie.whereToWatch')}</Mui.Typography>
                        </Mui.Button>
                    </Mui.Box>
                    <Mui.Box p={1} style={{ width: '70%' }}>
                        {displayLoadingMovieInformations()}
                    </Mui.Box>
                </Mui.Box>
                <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
                    <Mui.Box p={1}>
                        <Mui.Typography variant="h4" gutterBottom>{t('movie.cast')}:</Mui.Typography>
                    </Mui.Box>
                </Mui.Box>
                <LoadingCarousel cardKey={'cast'} slidesToShow={6}></LoadingCarousel>
                <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
                    <Mui.Box p={1}>
                        <Mui.Typography variant="h4" gutterBottom>{t('movie.followingsRatings')}:</Mui.Typography>
                    </Mui.Box>
                </Mui.Box>
                <LoadingCarousel cardKey={'followsRatings'} slidesToShow={6}></LoadingCarousel>
            </>
        );
    };

    const noMovie = () => {
        return (
            <Mui.Stack direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={3}
            >
                <Mui.Typography variant='h5'>{t('movie.noCorrespondingMovie')}</Mui.Typography>
            </Mui.Stack>
        );
    };

    return (
        <>
            {loading && displayLoading()}
            {!loading && !movie && noMovie()}
            {!loading && movie && displayMovie()}
        </>
    );
}
