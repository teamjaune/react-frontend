import * as React from 'react';
import * as Mui from '@mui/material';
import { useParams } from 'react-router-dom';
import { getAuth } from 'firebase/auth';
import Communication from '../service/communication';
import SearchMoviesTab from '../components/Search/SearchMoviesTab';
import SearchUsersTab from '../components/Search/SearchUsersTab';
import UserMovie from '../httpObjects/User/UserMovie';
import User from '../httpObjects/User';
import UserProfile from '../httpObjects/User/UserProfile';
import EditUser from '../components/User/EditUser';
import SearchLoadingTab from '../components/Search/SearchLoadingTab';
import { useTranslation } from 'react-i18next';

export default function UserPage() {
    const { id } = useParams();
    const [idSave, setIdSave] = React.useState(0);

    const [value, setValue] = React.useState(0);

    const [isFollowedSave, setIsFollowedSave] = React.useState(false);
    const [isFollowed, setIsFollowed] = React.useState(false);

    const [user, setUser] = React.useState(null);

    const [loading, setLoading] = React.useState(false);

    const [loadingMovies, setLoadingMovies] = React.useState(true);
    const [movies, setMovies] = React.useState([]);
    const [moviesOffset, setMoviesOffset] = React.useState(0);

    const [loadingFollowers, setLoadingFollowers] = React.useState(true);
    const [followers, setFollowers] = React.useState([]);
    const [followersOffset, setFollowersOffset] = React.useState(0);

    const [loadingFollowing, setLoadingFollowing] = React.useState(true);
    const [following, setFollowing] = React.useState([]);
    const [followingOffset, setFollowingOffset] = React.useState(0);

    const [noMoreMovies, setNoMoreMovies] = React.useState(false);
    const [noMoreFollowers, setNoMoreFollowers] = React.useState(false);
    const [noMoreFollowing, setNoMoreFollowing] = React.useState(false);

    const [openPopupFunction, createOpenPopupFunction] = React.useState(() => () => { });

    const [previousScrollLocation, setPreviousScrollLocation] = React.useState('');

    const [t] = useTranslation('common');

    React.useEffect(() => {
        if (id !== idSave) {
            setPreviousScrollLocation(0);
        }
        window.scrollTo({ top: previousScrollLocation, behavior: 'instant' });
        if (id !== idSave || isFollowed !== isFollowedSave) {
            setIdSave(id);
            setIsFollowedSave(isFollowed);
            fetchProfile();
            if (id !== idSave) {
                setMovies([]);
                setLoadingMovies(true);
                setNoMoreMovies(false);
                fetchMovies(true);

                setFollowing([]);
                setLoadingFollowing(true);
                setNoMoreFollowing(false);
                fetchFollowing(true);
            }
            if (id !== idSave || isFollowed !== isFollowedSave) {
                setFollowers([]);
                setLoadingFollowers(true);
                setNoMoreFollowers(false);
                fetchFollowers(true);
            }
        }
    }, [id, isFollowed, user, loadingMovies, moviesOffset, loadingFollowers, followersOffset, loadingFollowing, followingOffset]);

    const fetchProfile = () => {
        getAuth().currentUser.getIdToken().then(token => {
            Communication.sendGetRequest('api/private/user/' + (id || ''), token).then(async (response) => {
                const result = await response.json();
                if (result === null) {
                    setUser(null);
                } else {
                    const respUser = new UserProfile(result);
                    setUser(respUser);
                    setIsFollowed(result.is_followed);
                }
            }).catch((_) => {
                setUser(undefined);
            });
            setLoading(false);
        });
    };

    const handleChange = (_, newValue) => {
        setValue(newValue);
    };

    const clicked = () => {
        if (user.is_current) {
            edit();
        } else {
            follow();
        }
    };

    const edit = () => {
        openPopupFunction();
    };

    const follow = () => {
        getAuth().currentUser.getIdToken().then(token => {
            if (!isFollowed) {
                Communication.sendPutRequest('api/private/user/' + (id || '') + '/follow', token).then(async (response) => {
                    if (response.status === 200) {
                        changeUserFollowedDisplay();
                    }
                });
            } else {
                Communication.sendDeleteRequestV2('api/private/user/' + (id || '') + '/follow', token).then(async (response) => {
                    if (response.status === 200) {
                        changeUserFollowedDisplay();
                    }
                });
            }
        });
    };

    const changeUserFollowedDisplay = () => {
        setIsFollowed(!isFollowed);
    };

    const displayUser = () => {
        if (user === null || loading) {
            return <Mui.CircularProgress />;
        }
        if (user === undefined) {
            return <Mui.Typography style={{ textAlign: 'center' }} variant='h5'>{t('user.doesNotExist')}</Mui.Typography>
        }
        return (
            <Mui.Grid container direction='row' justifyContent='center'>
                <Mui.Grid item>
                    <Mui.Avatar
                        src={process.env.REACT_APP_SERVER_ADDRESS + '/api/public/images/profile/' + id || '/broken-image.jpg'}
                        sx={{ m: 4, minHeight: 50, minWidth: 50 }}
                        variant='rounded' />
                </Mui.Grid>
                <Mui.Grid item
                    alignSelf='center'
                >
                    <Mui.Typography variant='h5'>
                        {user.username}
                    </Mui.Typography>
                    <Mui.Typography variant='h6'>
                        {user.movie_count} {t('user.moviesRated')} | {user.followers_count} {t('user.followers')} | {user.following_count} {t('user.follows')}
                    </Mui.Typography>
                </Mui.Grid>
                <Mui.Grid item
                    alignSelf='center'
                    style={{ marginLeft: '20px' }}
                >
                    <Mui.Button variant='contained' onClick={clicked} color={(user.is_current || (!user.is_current && !isFollowed)) ? 'primary' : 'error'}>
                        {user.is_current && t('user.editProfile')}
                        {!user.is_current && !isFollowed && t('user.follow')}
                        {!user.is_current && isFollowed && t('user.unfollow')}
                    </Mui.Button>
                </Mui.Grid>
            </Mui.Grid>
        )
    };

    function TabPanel(props) {
        const { children, value, index, ...other } = props;

        return (
            <div
                role='tabpanel'
                hidden={value !== index}
                id={`simple-tabpanel-${index}`}
                aria-labelledby={`simple-tab-${index}`}
                {...other}
            >
                {value === index && (
                    <Mui.Box sx={{ p: 3 }}>
                        {children}
                    </Mui.Box>
                )}
            </div>
        );
    }

    const getPreviousResults = (reset, previous) => {
        if (reset) {
            return [];
        }
        return previous !== undefined && previous !== null ? previous : [];
    }

    const fetchMovies = (reset) => {
        setLoadingMovies(true);
        getAuth().currentUser.getIdToken().then(token => {
            const offset = reset ? 0 : moviesOffset;
            Communication.sendGetRequest('api/private/user/' + (id ? id + '/' : '') + 'movies/' + offset, token).then(async (response) => {
                const result = await response.json();
                if (result === null || !Array.isArray(result)) {
                    setMovies([]);
                    setLoadingMovies(false);
                } else {
                    const res = getPreviousResults(reset, movies);
                    const oldLength = res.length;
                    for (const movie of result) {
                        res.push(new UserMovie(movie));
                    }
                    if (res.length === oldLength) {
                        setNoMoreMovies(true);
                    }
                    setMovies(res);
                    const newOffset = offset + 10;
                    setMoviesOffset(newOffset);
                    setLoadingMovies(false);
                }
            }).catch((_) => {
                setMovies(undefined);
                setLoadingMovies(false);
            });
        });
    };

    const fetchFollowers = (reset) => {
        setLoadingFollowers(true);
        getAuth().currentUser.getIdToken().then(token => {
            const offset = reset ? 0 : followersOffset;
            Communication.sendGetRequest('api/private/user/' + (id ? id + '/' : '') + 'followers/' + offset, token).then(async (response) => {
                const result = await response.json();
                if (result === null || !Array.isArray(result)) {
                    setFollowers([]);
                    setLoadingFollowers(false);
                } else {
                    const res = getPreviousResults(reset, followers);
                    const oldLength = res.length;
                    for (const user of result) {
                        res.push(new User(user));
                    }
                    if (res.length === oldLength) {
                        setNoMoreFollowers(true);
                    }
                    setFollowers(res);
                    const newOffset = offset + 10;
                    setFollowersOffset(newOffset);
                    setLoadingFollowers(false);
                }
            }).catch((_) => {
                setFollowers(undefined);
                setLoadingFollowers(false);
            });
        });
    };

    const fetchFollowing = (reset) => {
        setLoadingFollowing(true);
        getAuth().currentUser.getIdToken().then(token => {
            const offset = reset ? 0 : followingOffset;
            Communication.sendGetRequest('api/private/user/' + (id ? id + '/' : '') + 'following/' + offset, token).then(async (response) => {
                const result = await response.json();
                if (result === null || !Array.isArray(result)) {
                    setFollowing([]);
                    setLoadingFollowing(false);
                } else {
                    const res = getPreviousResults(reset, following);
                    const oldLength = res.length;
                    for (const user of result) {
                        res.push(new User(user));
                    }
                    if (res.length === oldLength) {
                        setNoMoreFollowing(true);
                    }
                    setFollowing(res);
                    const newOffset = offset + 10;
                    setFollowingOffset(newOffset);
                    setLoadingFollowing(false);
                }
            }).catch((_) => {
                setFollowing(undefined);
                setLoadingFollowing(false);
            });
        });
    };

    const fetchMore = () => {
        setPreviousScrollLocation(window.scrollY);
        switch (value) {
            case 0:
                fetchMovies(false);
                break;
            case 1:
                fetchFollowers(false);
                break;
            case 2:
                fetchFollowing(false);
                break;
        }
    };

    return (
        <>
            {displayUser()}
            <EditUser user={user} fetchProfile={fetchProfile} createOpenPopupFunction={createOpenPopupFunction}></EditUser>
            {user !== null && user !== undefined &&
                <Mui.Box sx={{ width: '100%' }}>
                    <Mui.Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <Mui.Tabs value={value} onChange={handleChange} centered>
                            <Mui.Tab label={t('user.movies')} />
                            <Mui.Tab label={t('user.followers')} />
                            <Mui.Tab label={t('user.follows')} />
                        </Mui.Tabs>
                    </Mui.Box>
                    <TabPanel value={value} index={0}>
                        <SearchMoviesTab movies={movies} loading={loadingMovies} setLoading={setLoadingMovies}></SearchMoviesTab>
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        <SearchUsersTab users={followers} loading={loadingFollowers} setLoading={setLoadingFollowers}></SearchUsersTab>
                    </TabPanel>
                    <TabPanel value={value} index={2}>
                        <SearchUsersTab users={following} loading={loadingFollowing} setLoading={setLoadingFollowing}></SearchUsersTab>
                    </TabPanel>
                    <Mui.Grid
                        container
                        direction='row'
                        justifyContent='space-evenly'
                        alignItems='center'
                    >
                        {value === 0 && loadingMovies && <SearchLoadingTab tab={'movies'} displayRating={true}></SearchLoadingTab>}
                        {user.movie_count > 0 && value === 0 && !loadingMovies && !noMoreMovies && <Mui.Button onClick={fetchMore} variant="contained" style={{ width: '80%' }}>{t('global.seeMore')}</Mui.Button>}
                        {value === 1 && loadingFollowers && <SearchLoadingTab tab={'followers'} displayRating={true}></SearchLoadingTab>}
                        {user.followers_count > 0 && value === 1 && !loadingFollowers && !noMoreFollowers && <Mui.Button onClick={fetchMore} variant="contained" style={{ width: '80%' }}>{t('global.seeMore')}</Mui.Button>}
                        {value === 2 && loadingFollowing && <SearchLoadingTab tab={'following'} displayRating={true}></SearchLoadingTab>}
                        {user.following_count > 0 && value === 2 && !loadingFollowing && !noMoreFollowing && <Mui.Button onClick={fetchMore} variant="contained" style={{ width: '80%' }}>{t('global.seeMore')}</Mui.Button>}
                    </Mui.Grid>
                </Mui.Box>
            }
        </>
    );
}
