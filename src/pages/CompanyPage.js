import * as React from 'react';
import * as Mui from '@mui/material';
import { useParams } from 'react-router-dom';
import { getAuth } from 'firebase/auth';
import Communication from '../service/communication';
import Company from '../httpObjects/Company';
import MovieCard from '../components/Cards/MovieCard';
import HowToWatchCarousel from '../components/HowToWatchCarousel';
import LoadingCarousel from '../components/LoadingCarousel';
import { useTranslation } from 'react-i18next';
import ReactCountryFlag from 'react-country-flag';

export default function CompanyPage() {
    const { companyID } = useParams();

    const [company, setCompany] = React.useState(null);
    const [loading, setLoading] = React.useState(false);

    const [t] = useTranslation('common');

    React.useEffect(() => {
        if (companyID !== undefined && companyID !== null && companyID !== '') {
            setLoading(true);
            getAuth().currentUser.getIdToken().then(token => {
                Communication.sendGetRequest('api/private/companies/' + companyID, token).then(async (response) => {
                    const result = await response.json();
                    if (result === null || response.status !== 200) {
                        setCompany(null);
                        setLoading(false);
                    } else {
                        setCompany(new Company(result));
                        setLoading(false);
                    }
                });
            }).catch((_) => {
                setCompany(undefined);
                setLoading(false);
            });
        }
    }, [companyID]);

    const displayMovies = () => {
        const movies = [];
        for (const movieID in company.movies) {
            const movie = company.movies[movieID];
            movies.push(<MovieCard key={movie.movie_id} movie={movie}></MovieCard>);
        }
        return movies;
    };

    const displayCompany = () => {
        if (company === undefined) {
            return <><Mui.Stack direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={3}
            ><Mui.Typography variant='h5'>Server error.</Mui.Typography></Mui.Stack></>
        }
        if (company === null) {
            return <><Mui.Stack direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={3}
            ><Mui.Typography variant='h5'>Error.</Mui.Typography></Mui.Stack></>
        }

        let nbOfSlidesToScroll = 3;
        while (Object.keys(company.movies).length % nbOfSlidesToScroll !== 0) {
            nbOfSlidesToScroll--;
        }
        const settingsMovies = {
            infinite: true,
            slidesToShow: 6,
            slidesToScroll: nbOfSlidesToScroll
        };

        return (
            <>
                <Mui.Box display='flex' justifyContent='flex-start' mx={10} p={1}>
                    <Mui.Box p={1} style={{ width: '30%' }}>
                        {company.image_url && <img style={{ backgroundColor: 'white', width: '100%' }} src={company.image_url} />}
                        {!company.image_url && <img style={{ backgroundColor: 'white', width: '100%' }} src='/images/waiting.png' />}
                    </Mui.Box>
                    <Mui.Box p={1} style={{ width: '70%' }}>
                        <Mui.Typography style={{ marginBottom: '0' }} variant='h3' gutterBottom>
                            {company.name}
                        </Mui.Typography>
                        {
                            company.origin_country && <Mui.Typography variant='h6' gutterBottom>
                                {t('company.originCountry')}: {company.origin_country} <ReactCountryFlag style={{ marginRight: '5px' }} countryCode={company.origin_country} svg />
                            </Mui.Typography>
                        }
                        {
                            company.headquarters && <Mui.Typography variant='h6' gutterBottom>
                                {company.headquarters}
                            </Mui.Typography>
                        }
                        {
                            company.homepage && <Mui.Typography variant='h6' gutterBottom>
                                <a href={company.homepage}>{company.homepage}</a>
                            </Mui.Typography>
                        }
                        {
                            company.description && <Mui.Typography style={{ marginTop: '20px' }} variant='h4' gutterBottom>
                                Description
                            </Mui.Typography>}
                        {
                            company.description && <Mui.Typography variant='h6' gutterBottom>
                                {company.description}
                            </Mui.Typography>
                        }
                    </Mui.Box>
                </Mui.Box>
                <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
                    <Mui.Box p={1}>
                        <Mui.Typography variant="h4" gutterBottom>{t('company.movies')}:</Mui.Typography>
                    </Mui.Box>
                </Mui.Box>
                <Mui.Box display="block" mx={10} p={1}>
                    <HowToWatchCarousel {...settingsMovies}>
                        {displayMovies()}
                    </HowToWatchCarousel>
                </Mui.Box>
            </>
        );
    };

    const displayLoadingCompany = () => {
        return (
            <>
                <Mui.Box display='flex' justifyContent='flex-start' mx={10} p={1}>
                    <Mui.Box p={1} style={{ width: '30%' }}>
                        <img style={{ backgroundColor: 'white', width: '100%' }} src={'/images/waiting.png'} />
                    </Mui.Box>
                    <Mui.Box p={1} style={{ width: '70%' }}>
                        <Mui.Typography style={{ marginBottom: '0' }} variant='h3' gutterBottom>
                            <Mui.CircularProgress />
                        </Mui.Typography>
                        <Mui.Typography variant='h6' gutterBottom>
                            <Mui.CircularProgress />
                        </Mui.Typography>
                    </Mui.Box>
                </Mui.Box>
                <Mui.Box display="flex" justifyContent="flex-start" mx={10} p={1}>
                    <Mui.Box p={1}>
                        <Mui.Typography variant="h4" gutterBottom>{t('company.movies')}:</Mui.Typography>
                    </Mui.Box>
                </Mui.Box>
                <LoadingCarousel cardKey={'movies'} slidesToShow={6}></LoadingCarousel>
            </>
        );
    };

    const noCompany = () => {
        return (
            <Mui.Stack direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={3}
            >
                <Mui.Typography variant='h5'>{t('company.noCorrespondingMovie')}</Mui.Typography>
            </Mui.Stack>
        );
    };

    return (
        <>
            {loading && displayLoadingCompany()}
            {!loading && !company && noCompany()}
            {!loading && company && displayCompany()}
        </>
    );
}
