import * as React from 'react';
import GlobalSuggestions from '../components/Suggestions/GlobalSuggestions';
import LastRatedByFollows from '../components/Suggestions/LastRatedByFollows';
import LastRatedSuggestions from '../components/Suggestions/LastRatedSuggestions';

export default function MainPage() {
  return (
    <>
      <GlobalSuggestions key={'home_globalSuggestions'}></GlobalSuggestions>
      <LastRatedSuggestions key={'home_lastRatedSuggestions_1'} numberOfSuggestions={2} offset={0}></LastRatedSuggestions>
      <LastRatedByFollows key={'home_lastRatedByFollows'}></LastRatedByFollows>
      <LastRatedSuggestions key={'home_lastRatedSuggestions_2'} numberOfSuggestions={3} offset={2}></LastRatedSuggestions>
    </>
  );
}
