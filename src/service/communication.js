
const sendGetRequest = (endPoint, token) => {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` }
    };
    return fetch(`${process.env.REACT_APP_SERVER_ADDRESS}/${endPoint}`, requestOptions);
}

const sendPostRequest = (endPoint, token, object) => {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` },
        body: object
    };
    return fetch(`${process.env.REACT_APP_SERVER_ADDRESS}/${endPoint}`, requestOptions);
}

const sendFormPostRequest = (endPoint, token, object) => {
    const requestOptions = {
        method: 'POST',
        headers: { 'Authorization': `Bearer ${token}` },
        body: object
    };
    return fetch(`${process.env.REACT_APP_SERVER_ADDRESS}/${endPoint}`, requestOptions);
}

const sendPutRequest = (endPoint, token, object) => {
    const requestOptions = {
        method: 'Put',
        headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` },
        body: JSON.stringify(object)
    };
    return fetch(`${process.env.REACT_APP_SERVER_ADDRESS}/${endPoint}`, requestOptions);
}

const sendDeleteRequest = (endPoint, token, id) => {
    const requestOptions = {
        method: 'Delete',
        headers: { 'Authorization': `Bearer ${token}` }
    };
    return fetch(`${process.env.REACT_APP_SERVER_ADDRESS}/${endPoint}/${id}`, requestOptions);
}

const sendDeleteRequestV2 = (endPoint, token, object) => {
    const requestOptions = {
        method: 'Delete',
        headers: { 'Authorization': `Bearer ${token}` },
        body: JSON.stringify(object)
    };
    return fetch(`${process.env.REACT_APP_SERVER_ADDRESS}/${endPoint}`, requestOptions);
}

const sendGetRequestImage = (endPoint, token) => {
    const src = endPoint;
    const requestOptions = {
        method: 'GET',
        headers: { 'Authorization': `Bearer ${token}` }
    };
    return fetch(src, requestOptions);
};

export default { sendGetRequest, sendPostRequest, sendFormPostRequest, sendPutRequest, sendDeleteRequest, sendDeleteRequestV2, sendGetRequestImage }
