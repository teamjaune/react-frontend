import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import commonEn from './Translations/en/common.json';
import commonFr from './Translations/fr/common.json';

export const getDefaultLanguage = () => {
  if (!localStorage.getItem('language')) {
    let userLang = navigator.language || navigator.userLanguage;
    userLang = userLang.split('-')[0];
    if (userLang !== 'fr' && userLang !== 'en') {
      userLang = 'en';
    }
    localStorage.setItem('language', userLang);
  }
  return localStorage.getItem('language');
}

i18n
  .use(initReactI18next)
  .init({
    lng: getDefaultLanguage(),
    resources: {
      en: {
        common: commonEn
      },
      fr: {
        common: commonFr
      }
    },
    interpolation: {
      escapeValue: false
    }
  });
  export default i18n;
