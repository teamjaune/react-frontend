class SearchCompany {
    constructor(company) {
        this.company_id = company.company_id;
        this.name = company.name;
        this.description = company.description;
        this.image_url = company.image_url;
        this.origin_country = company.origin_country;
    }
}

export default SearchCompany;
