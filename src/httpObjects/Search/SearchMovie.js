class SearchMovie {
    constructor(movie) {
        this.movie_id = movie.movie_id;
        this.title_type = movie.title_type;
        this.primary_title = movie.primary_title;
        this.original_title = movie.original_title;
        this.is_adult = movie.is_adult;
        this.start_year = movie.start_year;
        this.end_year = movie.end_year;
        this.runtime_minutes = movie.runtime_minutes;
        this.our_rating = movie.our_rating;
        this.average_rating = movie.average_rating;
        this.number_votes = movie.number_votes;
        this.our_average_rating = movie.our_average_rating;
        this.image_url = movie.image_url;
        this.french_title = movie.french_title;
        this.synopsis = movie.synopsis;
    }

    getTitle() {
        const title = localStorage.getItem('language') === 'en' ? this.primary_title : this.french_title;
        if (title !== undefined && title !== null && title !== '') {
            return title;
        }
        return this.original_title;
    }

    getDuration() {
        const hours = Math.floor(this.runtime_minutes / 60);
        const minutes = Math.floor(this.runtime_minutes % 60);
        return (hours > 0 ? (hours + 'h ') : '') + minutes + 'm';
    }

    getNumberVotes() {
        if (this.number_votes > 999 && this.number_votes < 1000000) {
            return (this.number_votes / 1000).toFixed(1) + 'K';
        } else if (this.number_votes > 1000000) {
            return (this.number_votes / 1000000).toFixed(1) + 'M';
        } else if (this.number_votes < 900) {
            return this.number_votes;
        }
    }

    getTitleType() {
        switch (this.title_type) {
            case 'tvMiniSeries':
            case 'tvSeries':
                return 'Series';
            case 'tvShort':
            case 'movie':
            case 'tvMovie':
            case 'short':
            case 'video':
            case 'tvSpecial':
            default:
                return 'Movie';
        }
    }
}

export default SearchMovie;
