class SearchCrew {
    constructor(crew) {
        this.person_id = crew.person_id;
        this.name = crew.name;
        this.birth_year = crew.birth_year;
        this.death_year = crew.death_year;
        this.image_url = crew.image_url;
        this.biography = crew.biography;
    }
}

export default SearchCrew;
