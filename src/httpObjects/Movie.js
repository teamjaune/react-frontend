class Movie {
    constructor(movie) {
        this.movie_id = movie.movie_id;
        this.title_type = movie.title_type;
        this.primary_title = movie.primary_title;
        this.original_title = movie.original_title;
        this.is_adult = movie.is_adult;
        this.start_year = movie.start_year;
        this.end_year = movie.end_year;
        this.runtime_minutes = movie.runtime_minutes;
        this.user_rating = movie.user_rating;
        this.average_rating = movie.average_rating;
        this.number_votes = movie.number_votes;
        this.our_average_rating = movie.our_average_rating;
        this.image_url = movie.image_url;
        this.french_title = movie.french_title;
        this.synopsis = movie.synopsis;
        this.crew = this.concatCrew(movie);
        this.genres = movie.genres;
        this.keywords = movie.keywords;
        this.category = movie.category;
        this.categories = movie.categories;
        this.production_companies = movie.production_companies;
        this.watch_methods = movie.watch_methods;
        this.watch_methods_languages = movie.watch_methods_languages;
    }

    concatCrew(movie) {
        const res = [];
        if (movie.crew === undefined || movie.crew === null) {
            return res;
        }
        const crewMembers = {};
        for (const crew of movie.crew) {
            if (crewMembers[crew.person_id] === undefined || crewMembers[crew.person_id] === null) {
                crew.categories = [];
                crew.categories.push(crew.category);
                delete crew.category;
                crewMembers[crew.person_id] = crew;
            } else {
                crewMembers[crew.person_id].categories.push(crew.category);
            }
        }
        for (const crew in crewMembers) {
            res.push(crewMembers[crew]);
        }
        return res;
    }

    getTitle() {
        const title = localStorage.getItem('language') === 'en' ? this.primary_title : this.french_title;
        if (title !== undefined && title !== null && title !== '') {
            return title;
        }
        return this.original_title;
    }

    getDuration() {
        const hours = Math.floor(this.runtime_minutes / 60);
        const minutes = Math.floor(this.runtime_minutes % 60);
        return (hours > 0 ? (hours + 'h ') : '') + minutes + 'm';
    }

    getNumberVotes() {
        if (this.number_votes > 999 && this.number_votes < 1000000) {
            return (this.number_votes / 1000).toFixed(1) + 'K';
        } else if (this.number_votes > 1000000) {
            return (this.number_votes / 1000000).toFixed(1) + 'M';
        } else if (this.number_votes < 900) {
            return this.number_votes;
        }
    }

    getTitleType() {
        switch (this.title_type) {
            case 'tvMiniSeries':
            case 'tvSeries':
                return localStorage.getItem('language') === 'en' ? 'Serie' : 'Série';
            case 'tvShort':
            case 'movie':
            case 'tvMovie':
            case 'short':
            case 'video':
            case 'tvSpecial':
            default:
                return localStorage.getItem('language') === 'en' ? 'Movie' : 'Film';
        }
    }

    getMainWatchMethod() {
        if (!this.watch_methods || this.watch_methods.length === 0) {
            return null;
        }
        return this.watch_methods[0];
    }

    getWatchMethodsWithType(type) {
        const res = [];
        for (const watchMethod of this.watch_methods) {
            if (watchMethod.type === type) {
                res.push(watchMethod);
            }
        }
        return res;
    }
}

export default Movie;
