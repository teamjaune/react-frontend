class UserRating {
    constructor(userRating) {
        this.id = userRating.id;
        this.username = userRating.username;
        this.picture = userRating.picture;
        this.rating = userRating.rating;
    }
}

export default UserRating;
