import Movie from './Movie';

class Crew {
    constructor(crew) {
        this.person_id = crew.person_id;
        this.name = crew.name;
        this.birth_year = crew.birth_year;
        this.death_year = crew.death_year;
        this.image_url = crew.image_url;
        this.movies = this.concatMovies(crew);
        this.category = crew.category;
        this.categories = crew.categories;
        this.biography = crew.biography;
        this.birth_place = crew.birth_place;
    }

    concatMovies(crew) {
        const res = {};
        if (crew.movies === undefined || crew.movies === null) {
            return res;
        }
        for (const movie of crew.movies) {
            if (res[movie.movie_id] === undefined || res[movie.movie_id] === null) {
                movie.categories = [];
                movie.categories.push(movie.category);
                delete movie.category;
                res[movie.movie_id] = movie;
            } else {
                res[movie.movie_id].categories.push(movie.category);
            }
        }
        for (const movieID in res) {
            res[movieID] = new Movie(res[movieID]);
        }
        return res;
    }
}

export default Crew;
