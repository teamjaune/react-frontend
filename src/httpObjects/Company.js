import Movie from './Movie';

class Company {
    constructor(company) {
        this.company_id = company.company_id;
        this.name = company.name;
        this.description = company.description;
        this.image_url = company.image_url;
        this.origin_country = company.origin_country;
        this.headquarters = company.headquarters;
        this.homepage = company.homepage;
        this.movies = this.concatMovies(company);
    }

    concatMovies(company) {
        const res = [];
        if (company.movies === undefined || company.movies === null) {
            return res;
        }
        for (const movie of company.movies) {
            res.push(new Movie(movie));
        }
        return res;
    }
}

export default Company;
