class Rating {
    constructor(movieId, rating) {
        this.movieId = movieId;
        this.rating = rating;
    }
}

export default Rating;
