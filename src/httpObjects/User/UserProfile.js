class UserProfile {
    constructor(userProfile) {
        this.username = userProfile.username;
        this.picture = userProfile.picture;
        this.movie_count = userProfile.movie_count;
        this.followers_count = userProfile.followers_count;
        this.following_count = userProfile.following_count;
        this.is_followed = userProfile.is_followed;
        this.is_current = userProfile.is_current;
    }
}

export default UserProfile;
