class User {
    constructor(user) {
        this.id = user.id;
        this.firebase_id = user.firebase_id;
        this.username = user.username;
        this.email = user.email;
        this.picture = user.picture;
        this.issuer = user.issuer;
        this.locked = user.locked;
        this.enabled = user.enabled;
        this.password = user.password;
        this.authorities = user.authorities;
        this.accountNonLocked = user.accountNonLocked;
        this.credentialsNonExpired = user.credentialsNonExpired;
        this.accountNonExpired = user.accountNonExpired;
        this.is_self = user.is_self;
    }
}

export default User;
