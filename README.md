# Getting Started

```
git clone https://gitlab.com/teamjaune/react-frontend.git
cd react-frontend
npm install
echo .env
```
## Before Running 
Firebase must be initialized using a `.env` file placed in the project's root directory with the following contents :

```
REACT_APP_SERVER_ADDRESS=
REACT_APP_FIREBASE_API_KEY=
REACT_APP_FIREBASE_AUTH_DOMAIN=
REACT_APP_FIREBASE_PROJECT_ID=
REACT_APP_FIREBASE_STORAGE_BUCKET=
REACT_APP_FIREBASE_MESSAGING_SENDER_ID=
REACT_APP_FIREBASE_APP_ID= 
```
## Start the developement server
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
```
npm start
```

## Build the client
```
npm run build
```

## Check Syntax

```
npm run lint
```